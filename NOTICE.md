# Notices for Eclipse openK User Modules

This content is produced and maintained by the Eclipse openK User Modules project.

 * Project home: https://projects.eclipse.org/projects/technology.openk-usermodules

## Trademarks

Eclipse openK User Modules is a trademark of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/openk-usermodules
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.elogbook
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.elogbookFE
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.centralService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.homeService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.docu
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.frontend

## Third-party Content

antlr (4.13.0)
 * License: BSD 3-clause
 * Project: https://www.antlr.org/antlr4-runtime/
 * Source: https://mvnrepository.com/artifact/org.antlr/antlr4-runtime/4.13.0
 
asm 9.6)
 * License: BSD 3-clause
 * Project: http://asm.ow2.io/
 * Source: https://mvnrepository.com/artifact/org.ow2.asm/asm/9.6

logback-classic (1.4.14)
 * License: EPL 1.0 LGPL 2.1
 * Project:  http://logback.qos.ch/logback-classic
 * Source: https://mvnrepository.com/artifact/ch.qos.logback/logback-classic/1.4.14

logback-core (1.4.12)
 * License: EPL 1.0 LGPL 2.1
 * Project: http://logback.qos.ch/logback-core
 * Source: https://mvnrepository.com/artifact/ch.qos.logback/logback-core/1.4.12

proj4j (1.1.0)
 * License: Apache-2.0
 * Project: https://github.com/locationtech/proj4j
 * Source: https://mvnrepository.com/artifact/org.locationtech.proj4j/proj4j/1.1.0

jackson-annotations (2.15.4)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations/2.15.4

jackson-core (2.15.4)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core/2.15.4

jackson-databind (2.14.4)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind/2.15.4

jackson-datatype-jdk8 (2.15.4)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.datatype/jackson-datatype-jdk8/2.15.4

jackson-datatype-jsr310 (2.15.4)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.datatype/jackson-datatype-jsr310/2.15.4

jackson-module-parameter-names (2.15.4)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-parameter-names/2.15.4
 
xmlunit-core (2.9.1)
 * License: Apache-2.0
 * Project: https://www.xmlunit.org/
 * Source: https://mvnrepository.com/artifact/org.xmlunit/xmlunit-core/2.9.1 

classmate (1.6.0)
 * License: Apache-2.0
 * Project: https://github.com/FasterXML/java-classmate
 * Source: https://mvnrepository.com/artifact/com.fasterxml/classmate/1.6.0

h2 (2.1.214)
 * License: EPL-1.0, MPL-2.0
 * Project: https://h2database.com
 * Source: https://mvnrepository.com/artifact/com.h2database/h2/2.1.214 

jakarta.activation (2.1.3)
 * License: EDL-1.0
 * Project: https://github.com/jakartaee/jaf-api
 * Source: https://mvnrepository.com/artifact/jakarta.activation/jakarta.activation-api/2.1.3

istack-commons-runtime (4.1.2)
 * License: EDL-1.0
 * Project: https://projects.eclipse.org/projects/ee4j/istack-commons/istack-commons-runtime
 * Source: https://mvnrepository.com/artifact/com.sun.istack/istack-commons-runtime/4.1.2

jakarta.mail (2.0.3)
 * License: EDL-1.0, EPL-2.0, GPL
 * Project: http://eclipse-ee4j.github.io/angus-mail/jakarta.mail 
 * Source: https://mvnrepository.com/artifact/com.sun.mail/jakarta.mail/2.0.3

HikariCP (5.0.1)
 * License: Apache-2.0 
 * Project: https://github.com/brettwooldridge/HikariCP 
 * Source: https://mvnrepository.com/artifact/com.zaxxer/HikariCP/5.0.1

commons-fileupload (1.5)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/proper/commons-fileupload/ 
 * Source: https://mvnrepository.com/artifact/commons-fileupload/commons-fileupload/1.5

commons-io (2.11.0)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/io/ 
 * Source: https://mvnrepository.com/artifact/commons-io/commons-io/2.11.0

commons-lang (3.13.0)
 * License: Apache-2.0 
 * Project: https://commons.apache.org/proper/commons-lang/
 * Source: https://mvnrepository.com/artifact/commons-lang/commons-lang/3.13.0

commons-logging (1.2)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/proper/commons-logging/
 * Source: https://mvnrepository.com/artifact/commons-logging/commons-logging/1.2

feign-form-spring (3.8.0)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/io.github.openfeign.form/feign-form-spring/3.8.0

feign-form (3.8.0)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/io.github.openfeign.form/feign-form/3.8.0

feign-core (13.2.1)
 * License: Apache-2.0 
 * Project: https://github.com/openfeign/feign/feign-core
 * Source: https://mvnrepository.com/artifact/io.github.openfeign/feign-core/13.2.1

feign-slf4j (13.2.1)
 * License: Apache-2.0 
 * Project: https://github.com/openfeign/feign/feign-slf4j
 * Source: https://mvnrepository.com/artifact/io.github.openfeign/feign-slf4j/13.2.1

jjwt (0.9.1)
 * License: Apache-2.0 
 * Project: https://github.com/jwtk/jjwt
 * Source: https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt/0.9.1
 
swagger-ui (5.11.8)
 * License: Apache-2.0 
 * Project: http://webjars.org
 * Source: https://mvnrepository.com/artifact/org.webjars/swagger-ui/5.11.8

swagger-annotations-jakarta (2.2.20)
 * License: Apache-2.0 
 * Project: https://github.com/swagger-api/swagger-core/modules/swagger-annotations-jakarta
 * Source: https://mvnrepository.com/artifact/io.swagger.core.v3/swagger-annotations-jakarta/2.2.20
 
 swagger-core-jakarta (2.2.20)
 * License: Apache-2.0 
 * Project: https://github.com/swagger-api/swagger-core/modules/swagger-core-jakarta
 * Source: https://mvnrepository.com/artifact/io.swagger.core.v3/swagger-core-jakarta/2.2.20

swagger-models-jakarta (2.2.20)
 * License: Apache-2.0 
 * Project: https://github.com/swagger-api/swagger-core/modules/swagger-models-jakarta 
 * Source:  https://mvnrepository.com/artifact/io.swagger.core.v3/swagger-models-jakarta/2.2.20
 
junit-jupiter (5.10.2)
 * License: EPL-2.0
 * Project: https://junit.org/junit5/
 * Source: https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter/5.10.2
 
junit-jupiter-api (5.10.2)
 * License: EPL-2.0
 * Project: https://junit.org/junit5/
 * Source: https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api/5.10.2
 
junit-jupiter-engine (5.10.2)
 * License: EPL-2.0
 * Project: https://junit.org/junit5/
 * Source: https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine/5.10.2
 
junit-jupiter-params (5.10.2)
 * License: EPL-2.0
 * Project: https://junit.org/junit5/
 * Source: https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-params/5.10.2    
 
junit-platform-commons (1.10.2)
 * License: EPL-2.0
 * Project: https://junit.org/junit5/
 * Source: https://mvnrepository.com/artifact/org.junit.platform/junit-platform-commons/1.10.2   
 
junit-platform-engine (1.10.2)
 * License: EPL-2.0
 * Project: https://junit.org/junit5/
 * Source: https://mvnrepository.com/artifact/org.junit.platform/junit-platform-engine/1.10.2     
 
jakarta.inject-api (2.0.1)
 * License: EPL-2.0
 * Project: https://github.com/eclipse-ee4j/injection-api
 * Source: https://mvnrepository.com/artifact/jakarta.inject/jakarta.inject-api/2.0.1

jakarta.annotation-api (2.1.1)
 * License: EPL-2.0
 * Project: https://projects.eclipse.org/projects/ee4j.ca 
 * Source: https://mvnrepository.com/artifact/jakarta.annotation/jakarta.annotation-api/2.1.1

jakarta.persistence-api (3.1.0)
 * License: EDL-1.0, EPL-2.0
 * Project: https://github.com/eclipse-ee4j/jpa-api 
 * Source: https://mvnrepository.com/artifact/jakarta.persistence/jakarta.persistence-api/3.1.0

jakarta.transaction-api (2.0.1)
 * License: EPL-2.0
 * Project: https://projects.eclipse.org/projects/ee4j.jta 
 * Source: https://mvnrepository.com/artifact/jakarta.transaction/jakarta.transaction-api/2.0.1

jakarta.validation-api (3.0.2)
 * License: Apache-2.0 
 * Project: http://beanvalidation.org
 * Source: https://mvnrepository.com/artifact/jakarta.validation/jakarta.validation-api/3.0.2

jakarta.xml.bind-api (4.0.2)
 * License: EDL-1.0
 * Project: https://github.com/jakartaee/jaxb-api/jakarta.xml.bind-api
 * Source: https://mvnrepository.com/artifact/jakarta.xml.bind/jakarta.xml.bind-api/4.0.2

byte-buddy (1.14.17)
 * License: Apache-2.0 
 * Project: https://bytebuddy.net/byte-buddy
 * Source: https://mvnrepository.com/artifact/net.bytebuddy/byte-buddy/1.14.17
 
byte-buddy-agent (1.14.17)
 * License: Apache-2.0 
 * Project: https://bytebuddy.net/byte-buddy-agent
 * Source: https://mvnrepository.com/artifact/net.bytebuddy/byte-buddy-agent/1.14.17
 
accessors-smart (2.5.1)
 * License: Apache-2.0 
 * Project: https://urielch.github.io
 * Source: https://mvnrepository.com/artifact/net.minidev/accessors-smart/2.5.1
 
json-smart (2.5.1)
 * License: Apache-2.0 
 * Project: https://urielch.github.io
 * Source: https://mvnrepository.com/artifact/net.minidev/json-smart/2.5.1 
 
apiguardian-api (1.1.2)
 * License: Apache-2.0 
 * Project: https://github.com/apiguardian-team/apiguardian
 * Source: https://mvnrepository.com/artifact/org.apiguardian/apiguardian-api/1.1.2
 
assertj-core (3.24.2)
 * License: Apache-2.0 
 * Project: https://assertj.github.io/doc/#assertj-core
 * Source: https://mvnrepository.com/artifact/org.assertj/assertj-core/3.24.2
 
awaitility (4.2.1)
 * License: Apache-2.0 
 * Project: http://awaitility.org
 * Source: https://mvnrepository.com/artifact/org.awaitility/awaitility/4.2.1
 
bcprov-jdk18on (1.78)
 * License: Bouncy Castle Licence
 * Project: https://www.bouncycastle.org/java.html
 * Source: https://mvnrepository.com/artifact/org.bouncycastle/bcprov-jdk18on/1.78
 
checker-qual (3.5.0)
 * License: MIT License
 * Project: https://checkerframework.org
 * Source: https://mvnrepository.com/artifact/org.checkerframework/checker-qual/3.5.0
 
angus-activation (2.0.2)
 * License: EDL 1.0
 * Project: https://github.com/eclipse-ee4j/angus-activation/angus-activation
 * Source: https://mvnrepository.com/artifact/org.eclipse.angus/angus-activation/2.0.2
 
microprofile-openapi-api (3.1.1)
 * License: Apache-2.0
 * Project: https://github.com/eclipse-ee4j/angus-activation/angus-activation
 * Source: https://mvnrepository.com/artifact/org.eclipse.microprofile.openapi/microprofile-openapi-api/3.1.1

log4j-api (2.17.1)
 * License: Apache-2.0 
 * Project: https://logging.apache.org/log4j/2.x/log4j-api/
 * Source: https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api/2.17.1

log4j-to-slf4j (2.17.1)
 * License: Apache-2.0 
 * Project: https://logging.apache.org/log4j/2.x/log4j-to-slf4j/
 * Source: https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-to-slf4j/2.17.1

fontbox (2.0.6)
 * License: Apache-2.0 
 * Project: http://pdfbox.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.pdfbox/fontbox/2.0.6

pdfbox (2.0.6)
 * License: Apache-2.0 
 * Project: http://pdfbox.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.pdfbox/pdfbox/2.0.6

tomcat-embed-core (10.1.25)
 * License: Apache-2.0 
 * Project: https://tomcat.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.tomcat.embed/tomcat-embed-core/10.1.25

tomcat-embed-el (10.1.25)
 * License: Apache-2.0 
 * Project: https://tomcat.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.tomcat.embed/tomcat-embed-el/10.1.25

tomcat-embed-websocket (10.1.25)
 * License: Apache-2.0 
 * Project: https://tomcat.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.tomcat.embed/tomcat-embed-websocket/10.1.25

aspectjweaver (1.9.22)
 * License: EPL-2.0
 * Project: https://www.eclipse.org/aspectj/
 * Source: https://mvnrepository.com/artifact/org.aspectj/aspectjweaver/1.9.22

jaxb-runtime (4.0.5)
 * License: EDL-1.0
 * Project: https://eclipse-ee4j.github.io/jaxb-ri/
 * Source: https://mvnrepository.com/artifact/org.glassfish.jaxb/jaxb-runtime/4.0.5
 
jaxb-core (4.0.5)
 * License: EDL-1.0
 * Project: https://eclipse-ee4j.github.io/jaxb-ri/
 * Source: https://mvnrepository.com/artifact/org.glassfish.jaxb/jaxb-core/4.0.5 

txw2 (4.0.5)
 * License: EDL-1.0
 * Project: https://eclipse-ee4j.github.io/jaxb-ri/
 * Source: https://mvnrepository.com/artifact/org.glassfish.jaxb/txw2/4.0.5
 
hamcrest (2.2)
 * License: BSD 3-clause
 * Project: http://hamcrest.org/JavaHamcrest/
 * Source: https://mvnrepository.com/artifact/org.hamcrest/hamcrest/2.2 
 
hibernate-commons-annotations (6.0.6.Final)
 * License: LGPL-2.1
 * Project: http://hibernate.org
 * Source: https://mvnrepository.com/artifact/org.hibernate.common/hibernate-commons-annotations/6.0.6.Final

hibernate-core (6.4.9.Final)
 * License: LGPL-2.1
 * Project: http://hibernate.org/orm 
 * Source: https://mvnrepository.com/artifact/org.hibernate/hibernate-core/6.4.9.Final

jboss-logging (3.5.3.Final)
 * License: Apache-2.0
 * Project: http://www.jboss.org
 * Source: https://mvnrepository.com/artifact/org.jboss.logging/jboss-logging/3.5.3.Final

jandex (3.1.2)
 * License: Apache-2.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.jboss/jandex/3.1.2

keycloak-common (24.0.2)
 * License: Apache-2.0
 * Project: http://keycloak.org/keycloak-common
 * Source: https://mvnrepository.com/artifact/org.keycloak/keycloak-common/24.0.2

keycloak-core (24.0.2)
 * License: Apache-2.0
 * Project:  http://keycloak.org/keycloak-core
 * Source: https://mvnrepository.com/artifact/org.keycloak/keycloak-core/24.0.2

postgresql (42.3.6)
 * License: BSD-2-clause
 * Project: https://github.com/pgjdbc/pgjdbc 
 * Source: https://mvnrepository.com/artifact/org.postgresql/postgresql/42.3.6

lombok (1.18.32)
 * License: MIT
 * Project: https://projectlombok.org 
 * Source: https://mvnrepository.com/artifact/org.projectlombok/lombok/1.18.32
 
mockito-core (5.7.0)
 * License: MIT
 * Project: https://github.com/mockito/mockito 
 * Source: https://mvnrepository.com/artifact/org.mockito/mockito-core/5.7.0
 
mockito-junit-jupiter (5.7.0)
 * License: MIT
 * Project: https://github.com/mockito/mockito 
 * Source: https://mvnrepository.com/artifact/org.mockito/mockito-junit-jupiter/5.7.0

jul-to-slf4j (2.0.13)
 * License: MIT 
 * Project: http://www.slf4j.org 
 * Source: https://mvnrepository.com/artifact/org.slf4j/jul-to-slf4j/2.0.13

slf4j-api (2.0.13)
 * License: MIT 
 * Project: http://www.slf4j.org 
 * Source: https://mvnrepository.com/artifact/org.slf4j/slf4j-api/2.0.13
 
json-path (2.9.0)
 * License: Apache-2.0
 * Project: https://github.com/jayway/JsonPath
 * Source: https://mvnrepository.com/artifact/com.jayway.jsonpath/json-path/2.9.0 
 
objenesis (3.3)
 * License: Apache-2.0
 * Project: http://objenesis.org/objenesis
 * Source: https://mvnrepository.com/artifact/org.objenesis/objenesis/3.3
 
 opentest4j (1.3.0)
 * License: Apache-2.0
 * Project: https://github.com/ota4j-team/opentest4j
 * Source: https://mvnrepository.com/artifact/org.opentest4j/opentest4j/1.3.0
 
 evictor (1.0.0)
 * License: Apache-2.0
 * Project:	https://github.com/stoyanr/Evictor
 * Source: https://mvnrepository.com/artifact/com.stoyanr/evictor/1.0.0
 
 android-json (0.0.20131108.vaadin1)
 * License: Apache-2.0
 * Project:	http://developer.android.com/sdk
 * Source: https://mvnrepository.com/artifact/com.vaadin.external.google/android-json/0.0.20131108.vaadin1
 
 micrometer-commons (1.12.7)
 * License: Apache-2.0
 * Project:	https://github.com/micrometer-metrics/micrometer
 * Source: https://mvnrepository.com/artifact/io.micrometer/micrometer-commons/1.12.7
 
 micrometer-observation (1.12.7)
 * License: Apache-2.0
 * Project:	https://github.com/micrometer-metrics/micrometer
 * Source: https://mvnrepository.com/artifact/io.micrometer/micrometer-observation/1.12.7
 
 reactor-core (3.6.7)
 * License: Apache-2.0
 * Project:	https://github.com/reactor/reactor-core
 * Source: https://mvnrepository.com/artifact/io.projectreactor/reactor-core/3.6.7
 
 reactor-extra (3.5.1)
 * License: Apache-2.0
 * Project:	https://github.com/reactor/reactor-addons
 * Source: https://mvnrepository.com/artifact/io.projectreactor.addons/reactor-extra/3.5.1
 
 jsonassert (1.5.1)
 * License: Apache-2.0
 * Project:	 https://github.com/skyscreamer/JSONassert
 * Source: https://mvnrepository.com/artifact/org.skyscreamer/jsonassert/1.5.1
 
 reactive-streams (1.0.4)
 * License: MIT-0
 * Project:	http://www.reactive-streams.org/
 * Source: https://mvnrepository.com/artifact/org.reactivestreams/reactive-streams/1.0.4

spring-boot-autoconfigure (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-autoconfigure
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-autoconfigure/3.2.7

spring-boot-starter-jdbc (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-jdbc
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-jdbc/3.2.7

spring-boot-starter-json (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-json
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-json/3.2.7

spring-boot-starter-logging (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-logging
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-logging/3.2.7

spring-boot-starter-mail (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-mail
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-mail/3.2.7

spring-boot-starter-security (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-security
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-security/3.2.7

spring-boot-starter-tomcat (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-tomcat
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-tomcat/3.2.7

spring-boot-starter-web (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-web
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web/3.2.7

spring-boot-starter (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter/3.2.7

spring-boot (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot/3.2.7
 
 spring-boot-starter-aop (3.2.7)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-boot
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-aop/3.2.7
 
 spring-boot-starter-cache (3.2.7)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-boot
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-cache/3.2.7
 
 spring-boot-starter-data-jpa (3.2.7)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-boot
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-jpa/3.2.7
 
 spring-boot-starter-test (3.2.7)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-boot
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-test/3.2.7
 
 spring-boot-test (3.2.7)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-boot
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-test/3.2.7
 
 spring-boot-test-autoconfigure (3.2.7)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-boot
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-test-autoconfigure/3.2.7

spring-cloud-commons (4.1.3)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud/spring-cloud-commons/
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-commons/4.1.3

spring-cloud-context (4.1.3)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud/spring-cloud-context/
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-context/4.1.3

spring-cloud-openfeign-core (4.1.2)
 * License: Apache-2.0
 * Project: https://spring.io/spring-cloud/spring-cloud-openfeign/spring-cloud-openfeign-core
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-openfeign-core/4.1.2

spring-cloud-starter-openfeign 4.1.2)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-openfeign/4.1.2

spring-cloud-starter (4.1.3)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter/4.1.3
 
spring-cloud-loadbalancer (4.1.3)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud/spring-cloud-loadbalancer
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-loadbalancer/4.1.3
 
spring-cloud-starter-loadbalancer (4.1.3)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-loadbalancer/4.1.3 

spring-data-commons (3.2.7)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-data
 * Source: https://mvnrepository.com/artifact/org.springframework.data/spring-data-commons/3.2.7

spring-data-jpa (3.2.7)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-data-jpa
 * Source: https://mvnrepository.com/artifact/org.springframework.data/spring-data-jpa/3.2.7

spring-security-config (6.2.5)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-security
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-config/6.2.5

spring-security-core (6.2.5)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-security
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-core/6.2.5

spring-security-crypto (6.2.5)
 * License: Apache-2.0
 * License: Apache-2.0
 * Project:  https://spring.io/projects/spring-security
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-crypto/6.2.5

spring-security-rsa (1.1.3)
 * License: Apache-2.0
 * Project: http://github.com/spring-projects/spring-security-oauth
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-rsa/1.1.3

spring-security-web (6.2.5)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-security
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-web/6.2.5
 
spring-security-test (6.2.5)
 * License: Apache-2.0
 * Project: https://spring.io/projects/spring-security
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-test/6.2.5 

spring-aop (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-aop/6.1.10

spring-aspects (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-aspects6.1.10

spring-beans (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-beans/6.1.10
 
spring-context-support (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-context-support/6.1.10

spring-context (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-context/6.1.10

spring-core (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-core/6.1.10

spring-expression (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-expression/6.1.10

spring-jcl (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-jcl/6.1.10

spring-jdbc (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-jdbc/6.1.10

spring-orm (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-orm/6.1.10

spring-tx (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-tx/6.1.10

spring-web (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-web/6.1.10

spring-webmvc (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-webmvc/6.1.10
 
spring-test (6.1.10)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-test/6.1.10 
 
springdoc-openapi-starter-common (2.4.0)
 * License: Apache-2.0
 * Project: https://springdoc.org/springdoc-openapi-starter-common
 * Source: https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-starter-common/2.4.0
  
springdoc-openapi-starter-webmvc-api (2.4.0)
 * License: Apache-2.0
 * Project: https://springdoc.org/springdoc-openapi-starter-webmvc-api
 * Source: https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-starter-webmvc-api/2.4.0
  
springdoc-openapi-starter-webmvc-api (2.4.0)
 * License: Apache-2.0
 * Project:https://springdoc.org/springdoc-openapi-starter-webmvc-api
 * Source: https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-starter-webmvc-api/2.4.0

snakeyaml (2.2)
 * License: Apache-2.0
 * Project: http://www.snakeyaml.org/
 * Source: https://mvnrepository.com/artifact/org.yaml/snakeyaml/2.2


## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
