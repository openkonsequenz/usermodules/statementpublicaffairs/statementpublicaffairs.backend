/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.Constants;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestException;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.service.WorkflowDataService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.CommentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller provides end points to access the statement workflow-data.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class StatementWorkflowDataController {

	private static final String ERROR_OCCURRED_COLLECTING_WORKFLOW_CONTRIBUTIIONS = "Error occurred collecting workflow contributiions.";
	private static final String ACCESS_FORBIDDEN = "Access forbidden: ";
	private static final String STATEMENT_DOES_NOT_EXIST = "Statement does not exist, or no workflowdata set for statement.";

	@Autowired
	private WorkflowDataService workflowDataService;

	/**
	 * Get statement workflow data.
	 * 
	 * @param statementId statement identifier
	 * @return {@link WorkflowDataModel} of the specific statement
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/workflow")
	public WorkflowDataModel getStatementWorkflowData(@PathVariable final Long statementId) {
		try {
			Optional<WorkflowDataModel> oWorkflowData = workflowDataService.getWorkflowData(statementId);
			if (oWorkflowData.isPresent()) {
				return oWorkflowData.get();
			}
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Set statement workflow data.
	 * 
	 * @param statementId       statement identifier
	 * @param taskId            task identifier
	 * @param workflowDataModel {@link WorkflowDataModel} to set
	 * @return HTTP status
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/workflow")
	public ResponseEntity<Object> setStatementWorkflowData(@PathVariable final Long statementId,
			@PathVariable final String taskId, @RequestBody WorkflowDataModel workflowDataModel) {
		if (workflowDataModel == null || !workflowDataModel.isValid()) {
			throw new BadRequestException("Invalid workflow data.");
		}
		try {
			workflowDataService.setWorkflowData(statementId, taskId, workflowDataModel);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred storing new workflow data.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(ACCESS_FORBIDDEN + e.getMessage(), e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(
					"Statement does not exist, or no workflowdata set for statement. " + e.getMessage(), e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Invalid workflow data.", e);
		}
	}

	/**
	 * Get List of parent statement identifiers for specific statement.
	 * 
	 * @param statementId statement identifier
	 * @return List of parent statement identifiers
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/workflow/parents")
	public List<Long> getStatementParentIds(@PathVariable final Long statementId) {
		try {
			Optional<List<Long>> oParentIds = workflowDataService.getStatementParentIds(statementId);
			if (oParentIds.isPresent()) {
				return oParentIds.get();
			}
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Get List of child statement identifiers for specific statement.
	 * 
	 * @param statementId statement identifier
	 * @return List of child statement identifiers
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/workflow/children")
	public List<Long> getStatementChildrenIds(@PathVariable final Long statementId) {
		try {
			Optional<List<Long>> oParentIds = workflowDataService.getStatementChildrenIds(statementId);
			if (oParentIds.isPresent()) {
				return oParentIds.get();
			}
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Get Map of department contributions for specific statement.
	 * 
	 * @param statementId statement identifier
	 * @return Map of location to departments that already contributed.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/workflow/contributions")
	public Map<String, Set<String>> getStatementDepartmentContributions(@PathVariable final Long statementId) {
		try {
			Optional<Map<String, Set<String>>> oContributions = workflowDataService
					.getDepartmentContributions(statementId);
			if (oContributions.isPresent()) {
				return oContributions.get();
			}
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException(ERROR_OCCURRED_COLLECTING_WORKFLOW_CONTRIBUTIIONS, e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Set statement contributions.
	 * 
	 * @param statementId statement identifier
	 * @param taskId      task identifier
	 * @param contributed map of contributed location to departments
	 * @return HTTP status
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/workflow/contributions")
	public ResponseEntity<Object> setStatementDepartmentContributions(@PathVariable final Long statementId,
			@PathVariable final String taskId, @RequestBody final Map<String, Set<String>> contributed) {
		try {
			workflowDataService.setDepartmentContributions(statementId, taskId, contributed);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException(ERROR_OCCURRED_COLLECTING_WORKFLOW_CONTRIBUTIIONS, e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(ACCESS_FORBIDDEN + e.getMessage(), e);
		}
	}

	/**
	 * Set contribution for department the user is assigned to.
	 * 
	 * @param statementId statement identifier
	 * @param taskId      task identifier
	 * @return HTTP status
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PatchMapping(value = "/process/statements/{statementId}/task/{taskId}/workflow/contribute")
	public ResponseEntity<Object> getStatementDepartmentContributions(@PathVariable final Long statementId,
			@PathVariable final String taskId) {
		try {
			workflowDataService.setDepartmentUserContribute(statementId, taskId);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException(ERROR_OCCURRED_COLLECTING_WORKFLOW_CONTRIBUTIIONS, e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(ACCESS_FORBIDDEN + e.getMessage(), e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("User not member of a required division - " + e.getMessage(), e);
		}
	}

	/**
	 * Set parent statement identifiers.
	 * 
	 * @param statementId statement identifier
	 * @param taskId      task identifier
	 * @param parentIds   Set of statement identifiers
	 * @return HTTP status
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/workflow/parents")
	public ResponseEntity<Object> setStatementParentIds(@PathVariable final Long statementId,
			@PathVariable final String taskId, @RequestBody final Set<Long> parentIds) {
		try {
			workflowDataService.setStatementParents(statementId, taskId, parentIds);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred setting parents.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(ACCESS_FORBIDDEN + e.getMessage(), e);
		}

	}

	/**
	 * Get department configuration for the specific statement.
	 * 
	 * @param statementId statement identifier
	 * @return {@link StatementDepartmentsModel} department configuration
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/departmentconfig")
	public StatementDepartmentsModel getStatementDepartmentsReference(@PathVariable final Long statementId) {
		try {
			Optional<StatementDepartmentsModel> oDepartmentsModel = workflowDataService.getDepartments(statementId);
			if (oDepartmentsModel.isPresent()) {
				return oDepartmentsModel.get();
			}
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Get comments for statement.
	 * 
	 * @param statementId statement identifier
	 * @return List of {@link CommentModel} comments.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/comments")
	public List<CommentModel> getComments(@PathVariable final Long statementId) {
		try {
			Optional<List<CommentModel>> oComments = workflowDataService.getComments(statementId);
			if (oComments.isPresent()) {
				return oComments.get();
			}
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);

		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Add a new comment.
	 * 
	 * @param statementId statement identifier
	 * @param commentText comment text.
	 * @return
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PutMapping(value = "/process/statements/{statementId}/comments")
	public ResponseEntity<Object> putComment(@PathVariable final Long statementId, @RequestBody String commentText) {
		try {
			workflowDataService.addComment(statementId, commentText);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred adding a new comment.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Delete comment.
	 * 
	 * @param statementId statement identifier
	 * @param commentId   comment identifier
	 * @return HTTP status
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@DeleteMapping(value = "/process/statements/{statementId}/comments/{commentId}")
	public ResponseEntity<Object> disableComment(@PathVariable final Long statementId, @PathVariable Long commentId) {
		try {
			workflowDataService.disableComment(statementId, commentId);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred deleting comment.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException(ACCESS_FORBIDDEN + e.getMessage(), e);
		}
	}

	/**
	 * Update comment.
	 * 
	 * @param statementId statement identifier
	 * @param commentId   comment identifier
	 * @param commentText comment text.
	 * @return
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/comments/{commentId}")
	public ResponseEntity<Object> updateComment(@PathVariable final Long statementId, @PathVariable Long commentId,
			@RequestBody String commentText) {
		try {
			workflowDataService.updateComment(statementId, commentId, commentText);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_DOES_NOT_EXIST);
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred updating a new comment.", e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

}
