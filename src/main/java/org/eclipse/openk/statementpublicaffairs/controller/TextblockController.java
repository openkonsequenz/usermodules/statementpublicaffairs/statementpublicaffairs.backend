/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.Constants;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestException;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.service.TextblockService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TextConfiguration;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * REST controller provides end points to access the statement response text
 * configuration and arrangement.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class TextblockController {

	private static final String TEST_TEXTARRANGEMENT_JSON = "test-textarrangement.json";
	private static final String STATEMENT_WORKFLOW_DOES_NOT_EXIST_YET = "Statement Workflow does not exist yet.";

	@Autowired
	private TextblockService textblockService;

	/**
	 * Get {@link TextConfiguration} of a statement. Usually the latest version from
	 * when the statement process was started.
	 * 
	 * @param statementId statement identifier
	 * @return {@link TextConfiguration}
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/workflow/textblockconfig")
	public TextConfiguration getTextblockConfiguration(@PathVariable final Long statementId) {
		try {
			Optional<TextConfiguration> oDefinition = textblockService.getTextblockConfiguration(statementId);
			if (oDefinition.isPresent()) {
				return oDefinition.get();
			} else {
				throw new NotFoundException("Statement does not exist");
			}
		} catch (ForbiddenServiceException | InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing textblock config request.", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_WORKFLOW_DOES_NOT_EXIST_YET, e);
		}
	}

	/**
	 * Get current text arrangement of the specific statement.
	 * 
	 * @param statementId statement identifier
	 * @return List of {@link Textblock}s
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/process/statements/{statementId}/workflow/textarrangement")
	public List<Textblock> getTextArrangement(@PathVariable final Long statementId) {
		try {
			Optional<List<Textblock>> oTextArrangement = textblockService.getTextArrangement(statementId);
			if (oTextArrangement.isPresent()) {
				return oTextArrangement.get();
			}
			throw new NotFoundException("Statement does not exist");
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}

	}

	/**
	 * Set current text arrangement of the specific statement.
	 * 
	 * @param statementId     statement identifier
	 * @param taskId          task identifier
	 * @param textArrangement text arrangement
	 * @return HTTP status
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/task/{taskId}/workflow/textarrangement")
	public ResponseEntity<Object> setTextArrangement(@PathVariable final Long statementId,
			@PathVariable final String taskId, @RequestBody List<Textblock> textArrangement) {
		try {
			textblockService.setTextArrangement(statementId, taskId, textArrangement);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error occurred processing set text arrangement request.", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_WORKFLOW_DOES_NOT_EXIST_YET, e);
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException("Set text arrangement is forbidden.", e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Bad Request. " + e.getMessage(), e);
		}
	}

	/**
	 * Validate text arrangement.
	 * 
	 * @param statementId     statement identifier
	 * @param textArrangement text arrangement to validate
	 * @return {@link ValidationResult} containing references to invalid text blocks
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/workflow/textarrangement/validate")
	public ValidationResult validateTextArrangenent(@PathVariable final Long statementId,
			@RequestBody List<Textblock> textArrangement) {
		try {
			return textblockService.validate(statementId, textArrangement);
		} catch (InternalErrorServiceException | ForbiddenServiceException e) {
			throw new InternalServerErrorException("Error occurred processing compile text arrangement request.", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_WORKFLOW_DOES_NOT_EXIST_YET, e);
		}
	}

	/**
	 * Compile text arrangement to PDF document.
	 * 
	 * @param statementId     statement identifier
	 * @param textArrangement text arrangement
	 * @return PDF document
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/process/statements/{statementId}/workflow/textarrangement/compile")
	public ResponseEntity<Object> compileTextArrangement(@PathVariable final Long statementId,
			@RequestBody List<Textblock> textArrangement) {
		try {
			return pdfCompile(statementId, textArrangement);
		} catch (InternalErrorServiceException | ForbiddenServiceException e) {
			throw new InternalServerErrorException("Error occurred processing compile text arrangement request.", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_WORKFLOW_DOES_NOT_EXIST_YET, e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Invalid text arrangement - " + e.getMessage(), e);
		}
	}

	/**
	 * Test interface that compiles a standard text. Can be used to test new
	 * response templates.
	 * 
	 * @param statementId statement identifier
	 * @return PDF document
	 */
	@GetMapping(value = "/process/statements/{statementId}/workflow/textarrangement/compile-test")
	public ResponseEntity<Object> compileTestTextArrangement(@PathVariable final Long statementId) {
		try {
			String testTextArrangementPath = TEST_TEXTARRANGEMENT_JSON;
			List<Textblock> textArrangement = readTextTextArrangement(testTextArrangementPath);

			return pdfCompile(statementId, textArrangement);
		} catch (InternalErrorServiceException | ForbiddenServiceException e) {
			throw new InternalServerErrorException("Error occurred processing compile test request.", e);
		} catch (NotFoundServiceException e) {
			throw new NotFoundException(STATEMENT_WORKFLOW_DOES_NOT_EXIST_YET, e);
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Invalid text arrangement - " + e.getMessage(), e);
		}
	}

	private ResponseEntity<Object> pdfCompile(final Long statementId, List<Textblock> textArrangement)
			throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException,
			BadRequestServiceException {
		ValidationResult validationResult = textblockService.validate(statementId, textArrangement);
		if (!validationResult.isValid()) {
			return new ResponseEntity<>(validationResult, HttpStatus.valueOf(424));
		}

		AttachmentFile file = textblockService.compileTextArrangement(statementId, textArrangement);

		return ResponseEntity.ok().contentLength(file.getLength()).contentType(MediaType.valueOf(file.getType()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName() + "")
				.body(new InputStreamResource(file.getRessource()));
	}

	private List<Textblock> readTextTextArrangement(String testTextArrangementPath) {
		try {
			File file = ResourceUtils.getFile("classpath:" + testTextArrangementPath);
			ObjectMapper objectMapper = new ObjectMapper();
			Textblock[] arrangements = objectMapper.readValue(file, Textblock[].class);
			return Arrays.asList(arrangements);
		} catch (IOException e) {
			throw new InternalServerErrorException("Could not load templateConfig from file " + testTextArrangementPath,
					e);
		}
	}

}
