/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import java.util.List;
import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.Constants;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestException;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.FailedDependencyException;
import org.eclipse.openk.statementpublicaffairs.exceptions.FailedDependencyServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.UserAdminModel;
import org.eclipse.openk.statementpublicaffairs.model.UserAdminSettingsModel;
import org.eclipse.openk.statementpublicaffairs.service.AdminService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller provides admin end points.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class AdminController {

	@Autowired
	private AdminService adminService;

	/**
	 * Synchronize authNAuth users with role SPA_ACCESS to the users database.
	 * 
	 * @return OK_NoContent if successful.
	 * @throws InternalErrorServiceException when error occurred
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/admin/users-sync")
	public ResponseEntity<Object> syncUsers() {
		try {
			adminService.syncUsers();
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Internal Server Error - " + e.getMessage());
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Get current department and division structure.
	 * 
	 * @return DepartmentStructure
	 * @throws InternalErrorServiceException when error occurred
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/admin/departments")
	public Map<String, DistrictDepartmentsModel> getDepartments() {
		return adminService.getDepartmentStructure();
	}

	/**
	 * Set current department and division structure.
	 * 
	 * @return OK_NoContent if successful.
	 * @throws InternalErrorServiceException when error occurred
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PutMapping(value = "/admin/departments")
	public ResponseEntity<Object> setDepartments(
			@RequestBody Map<String, DistrictDepartmentsModel> departmentStructure) {
		try {
			adminService.setDepartmentStructure(departmentStructure);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Invalid department structure");
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}
	
	/**
	 * Get current textblock definition.
	 * 
	 * @return TextblockDefinition
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/admin/textblockconfig")
	public TextblockDefinition getTextblockDefinition() {
		return adminService.getTextblockDefinition();
	}

	/**
	 * Set textblock definition.
	 * 
	 * @return OK_NoContent if successful.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PutMapping(value = "/admin/textblockconfig")
	public ResponseEntity<Object> setTextblockDefinition(
			@RequestBody TextblockDefinition textblockDefinition) {
		try {
			adminService.setTextblockDefinition(textblockDefinition);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Invalid textblock config");
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		}
	}

	/**
	 * Get all users.
	 * 
	 * @return  list of all users
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@GetMapping(value = "/admin/users")
	public List<UserAdminModel> getUsers() {
		try {
			return adminService.getAllUsers();
		} catch (InternalErrorServiceException e) {
			throw new InternalServerErrorException("Error - " + e.getMessage());
		}
	}
	
	/**
	 * Set textblock definition.
	 * 
	 * @return OK_NoContent if successful.
	 * @throws NotFoundServiceException 
	 * @throws FailedDependencyServiceException 
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/admin/users/{userId}/settings")
	public ResponseEntity<Object> setUserSettings(@PathVariable final Long userId, @RequestBody final UserAdminSettingsModel settings) {
		try {
			adminService.setUserSettings(userId, settings);
			return Constants.RESPONSE_OK_NO_CONTENT;
		} catch (BadRequestServiceException e) {
			throw new BadRequestException("Invalid user settings - " + e.getMessage());
		} catch (ForbiddenServiceException e) {
			throw new ForbiddenException();
		} catch (FailedDependencyServiceException e) {
			throw new FailedDependencyException("Dpendency error - " + e.getMessage());
		} catch (NotFoundServiceException e) {
			throw new NotFoundException("Not found - " + e.getMessage());
		}
	}

}
