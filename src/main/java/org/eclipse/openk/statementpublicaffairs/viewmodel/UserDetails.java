/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.viewmodel;

import lombok.Data;

@Data
public class UserDetails {
    private String firstName;
    private String lastName;
    private String color;
    
    public static UserDetails userDetailsOf(String firstName, String lastName, String color) {
        UserDetails ud =  new UserDetails();
        ud.setFirstName(firstName);
        ud.setLastName(lastName);
        ud.setColor(color);
        return ud;
    }
}

