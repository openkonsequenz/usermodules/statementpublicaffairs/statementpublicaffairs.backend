/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.viewmodel;

import java.util.Map;

import lombok.Data;

@Data
public class StatementDraftBlockModel {
	private String type;
	private Long textBlockRefId;
	private Map<String,String> freeTextPlaceHolder;
	private Map<String,String> optionTextPlaceHolder;
	private Map<String,String> derrivedTextPlaceHolder;
	private String replacement;
}

