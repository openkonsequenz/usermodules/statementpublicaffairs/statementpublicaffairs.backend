/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.viewmodel;

import java.util.List;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Data
public class AttachmentModel {
	private Long id;
	private String name;
	private String type;
	private Long size;
	private String timestamp;
	private List<String> tagIds;

	public void setName(String name) {
		this.name = filter(name);
	}

	private String filter(String in) {

		if (in == null) {
			return null;
		}

		in = in.replaceAll("[^a-z A-Z0-9.äÄöÖüÜß_-]", "_");

		return in.replace("__", "_");
	}
}
