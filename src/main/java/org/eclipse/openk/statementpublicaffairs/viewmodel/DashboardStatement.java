/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.viewmodel;

import java.util.List;

import lombok.Data;

@Data
public class DashboardStatement {
	private StatementDetailsModel info;
	private List<StatementTaskModel> tasks;
	private Boolean editedByMe;
	private int mandatoryDepartmentsCount;
	private int mandatoryContributionsCount;
	private Boolean optionalForMyDepartment;
	private Boolean completedForMyDepartment;
	private Boolean departmentStandIn;
}

