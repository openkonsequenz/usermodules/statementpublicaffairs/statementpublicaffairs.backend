/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.statementpublicaffairs.util;

import java.util.List;
import java.util.UUID;

import org.eclipse.openk.statementpublicaffairs.model.Textblock;

/**
 * StatementDraftUtils utility class to check and streamline statement draft
 * text arrangements.
 * 
 * @author Tobias Stummer
 *
 */
public class StatementDraftUtils {

	/**
	 * Utility no public constructor.
	 */
	private StatementDraftUtils() {
	}

	public static void fillEmptyTextblockUids(List<Textblock> textArrangement) {
		for (Textblock block : textArrangement) {
			switch (block.getType()) {
			case block:
				break;
			case text:
			case newline:
			case pagebreak:
				if (block.getTextblockId() == null || block.getTextblockId().isEmpty()) {
					block.setTextblockId(UUID.randomUUID().toString());
				}
				break;
			}
		}
	}

	public static boolean equalIgnoreBothNull(List<Textblock> orig, List<Textblock> other) {
		if (orig == null && other == null) {
			return true;
		} else if (orig == null || other == null || orig.size() != other.size()) {
			return false;
		}
		return orig.equals(other);
	}

}
