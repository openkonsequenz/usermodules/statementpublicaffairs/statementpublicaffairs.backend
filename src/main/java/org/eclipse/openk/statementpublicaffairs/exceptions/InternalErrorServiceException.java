/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.exceptions;

/**
 * @author Tobias Stummer
 *
 */
public class InternalErrorServiceException  extends Exception {
   
    
private static final String DEFAULT_MESSAGE = "Error in internal service.";
    
    public InternalErrorServiceException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Create InternalErrorServiceException with own message. 
     * @param message Message
     */
    public InternalErrorServiceException(String message) {
        super(message);
    }
    
    /**
     * Create InternalErrorServiceException with own message and cause.
     * @param message Message
     * @param cause Cause
     */
    public InternalErrorServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
