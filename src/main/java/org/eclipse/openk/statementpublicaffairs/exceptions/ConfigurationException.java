/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.exceptions;

public class ConfigurationException extends Exception {

	private static final String DEFAULT_MESSAGE = "Invalid configuration";

	public ConfigurationException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * Create ConfigurationException with own message.
	 * 
	 * @param message Message
	 */
	public ConfigurationException(String message) {
		super(message);
	}

	/**
	 * Create ConfigurationException with own message and cause.
	 * 
	 * @param message Message
	 * @param cause   Cause
	 */
	public ConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}
}
