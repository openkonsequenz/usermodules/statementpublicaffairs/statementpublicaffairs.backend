/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Throw exception to create not implemented yet Http responses.
 * 
 * @author Tobias Stummer
 *
 */
@ResponseStatus(code = HttpStatus.NOT_IMPLEMENTED)
public class NotImplementedYetException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "Requested ressource is not implemnented yet.";
    
    public static final NotImplementedYetException EXCEPTION_NOT_IMPLEMENTED_YET = new NotImplementedYetException();

    public NotImplementedYetException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Create NotImplementedYetException with own message.
     * 
     * @param message Message
     */
    public NotImplementedYetException(String message) {
        super(message);
    }
}
