/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.statementpublicaffairs.config.auth;

import java.io.IOException;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;

import org.eclipse.openk.statementpublicaffairs.api.AuthNAuthApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import feign.FeignException;
import feign.Response;
import lombok.extern.java.Log;

@Log
@Component
public class JwtTokenValidationFilter extends OncePerRequestFilter {
	@Autowired
	private AuthNAuthApi authNAuthApi;

	@Value("${jwt.useStaticJwt}")
	private boolean useStaticJwt;

	@Value("${jwt.tokenHeader}")
	private String tokenHeader;

	@Value("${jwt.retryCount:3}")
	private int errorRetryCount;

	@Value("${jwt.retrySleepMs:1000}")
	private int retrySleepMs;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String authenticationHeader = useStaticJwt ? null : request.getHeader(this.tokenHeader);

		if (authenticationHeader != null) {
			final String bearerTkn = authenticationHeader.replace("Bearer ", "");
			if (!verifyToken(bearerTkn)) {
				final HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(response);
				wrapper.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token expired or not valid");
				chain.doFilter(request, wrapper.getResponse());
				return;
			}
		}
		chain.doFilter(request, response);
	}

	/**
	 * Verify token by authNAuth service with retries in case of non conclusive
	 * service responses.
	 * 
	 * @param bearerTkn token
	 * @return true if valid, otherwise false
	 */
	private boolean verifyToken(final String bearerTkn) {
		int retrys = errorRetryCount;
		Boolean valid = null;
		while (valid == null) {
			int resStatusCode = -1;
			try {
				Response res = authNAuthApi.isTokenValid(bearerTkn);
				resStatusCode = res.status();				
			} catch (FeignException e) {
				log.warning("Exception on token verification: " + e.getMessage());
			}
			switch (resStatusCode) {
			case 200:
				valid = true;
				break;
			case 401:
				valid = false;
				break;
			default:
				log.warning("Could not verify token by authNAuth: got response status " + resStatusCode);
				retrys--;
				if (retrys < 0) {
					log.warning("Could not verify token by authNAuth: retry limit was reached");
					valid = false;
				} else {
					log.warning("Retry ... " + retrys);
					try {
						Thread.sleep(retrySleepMs);
					} catch (InterruptedException e) {
						log.warning("Retry sleep interrupted");
						Thread.currentThread().interrupt();
					}
				}
			}
		}
		return Boolean.TRUE.equals(valid);
	}
}
