/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.conf;

import java.util.Optional;

import lombok.Data;

@Data
public class Rule {
	public static final String ANY = "ANY";

	private String taskDefinition;
	private String role;
	private String action;
	private String optionalConstraint;
	
	public static Optional<Rule> ofParameters(String[] parameters) {
		if (parameters == null || parameters.length != 4) {
			return Optional.empty();
		}
		for (String s : parameters) {
			if (s.trim().length() == 0) {
				return Optional.empty();
			}
		}

		Rule rule = new Rule();
		rule.setTaskDefinition(parameters[0].trim());
		rule.setRole(parameters[1].trim());
		rule.setAction(parameters[2].trim());
		rule.setOptionalConstraint(parameters[3].trim());
		return Optional.of(rule);
	}
}


