/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.camunda;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Data
public class CamundaProcessInstance {

	private String id;
	private String processDefinitionId;
	private String processDefinitionKey;
	private String processDefinitionName;
	private String processDefinitionVersion;
	private String businessKey;
	private String caseInstanceId;
	private String startTime;
	private String endTime;
	private Long durationInMillis;
	private String tenantId;
	private String state;

}
