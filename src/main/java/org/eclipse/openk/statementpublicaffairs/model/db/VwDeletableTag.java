/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import org.springframework.data.annotation.Immutable;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Entity
@Immutable
@Data
public class VwDeletableTag {

	@Id
	@Column(name = "id")
	private String id;

	private String name;
	private Boolean standard;
	private Boolean disabled;
}
