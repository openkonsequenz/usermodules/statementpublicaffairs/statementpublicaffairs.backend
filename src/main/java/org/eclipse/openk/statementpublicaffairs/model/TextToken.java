/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import org.eclipse.openk.statementpublicaffairs.service.compile.Token;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextToken {

	private Token type;
	private String value;

	private static final String TEXT_BULLET_NL_SPACE = "       ";

	public static TextToken newLineTextToken() {
		TextToken token = new TextToken();
		token.setType(Token.TK_NL);
		token.setValue("\n");
		return token;
	}

	public static TextToken pagebreakTextToken() {
		TextToken token = new TextToken();
		token.setType(Token.TK_PB);
		token.setValue("<PB>");
		return token;
	}

	public static TextToken newBulletShiftTextToken() {
		TextToken token = new TextToken();
		token.setType(Token.STRING);
		token.setValue(TEXT_BULLET_NL_SPACE);
		return token;
	}

	public static TextToken newSpaceTextToken() {
		TextToken token = new TextToken();
		token.setType(Token.TK_SPACE);
		token.setValue(" ");
		return token;
	}

	public static TextToken newBulletToken() {
		TextToken token = new TextToken();
		token.setType(Token.TK_BULLET);
		token.setValue(" *");
		return token;
	}

	public static TextToken newBoldToken() {
		TextToken token = new TextToken();
		token.setType(Token.TK_BOLD);
		token.setValue("**");
		return token;
	}

	public static TextToken newItalicToken() {
		TextToken token = new TextToken();
		token.setType(Token.TK_ITALIC);
		token.setValue("__");
		return token;
	}


}
