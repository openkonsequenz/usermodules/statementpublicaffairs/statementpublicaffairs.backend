/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;


import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Entity
@Data
public class TblWorkflowdata {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false)
	private long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statement_id")
	private TblStatement statement;

	@Column(name = "pos")
	private String position;
	
	private Boolean initialState;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "textblockdefinition_id")
	private TblTextblockdefinition textBlockDefinition;
	
	@OneToMany(mappedBy = "workflowdata", fetch = FetchType.LAZY)
	private List<TblComment> comments;
	
    @OneToMany(mappedBy= "workflowdata", fetch=FetchType.LAZY)
	private List<TblReqdepartment> requiredDepartments;

}
