/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.springframework.data.annotation.Immutable;

import lombok.Data;

@Immutable
@Data
@Entity
public class VwStatementdraftHistory {

	@Id
	private Long id;

	@Column(name = "v")
	private Long version;
	private Long statementId;
	private Long workflowId;

	private Long userId;
	private String username;
	private String UserFirstName;
	private String UserLastName;

	@Column(name = "ts", columnDefinition = "TIMESTAMP")
	private LocalDateTime timestamp;
	private String msg;

	@Convert(converter = TextblockArrangementConverter.class)
	private List<Textblock> draft;

}
