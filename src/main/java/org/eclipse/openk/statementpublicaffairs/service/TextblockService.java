/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementdraft;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTextblockdefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblWorkflowdata;
import org.eclipse.openk.statementpublicaffairs.repository.StatementDraftRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.service.compile.TextCompileUtil;
import org.eclipse.openk.statementpublicaffairs.util.StatementDraftUtils;
import org.eclipse.openk.statementpublicaffairs.util.Time;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TextConfiguration;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ValidationError;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * TextblockService for text block operations. It provides interfaces to get and
 * set the current statement specific text configuration as well as interfaces
 * to validate and compile a text block configuration.
 * 
 * @author Tobias Stummer
 *
 */
@Service
public class TextblockService {

	private static final String INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK = "Invalid text arrangement format. Text block ";

	private static final String STATEMENT_WITH_GIVEN_IDENTIFIER_COULD_NOT_BE_FOUND = "Statement with given identifier could not be found.";

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private StatementCompileService statementCompileService;

	@Autowired
	private StatementService statementService;

	@Autowired
	private StatementDraftRepository statementDraftRepository;

	@Autowired
	UsersService usersService;

	@Value("${statement.compile.dateFormatPattern:\"dd.MM.yyyy\"}")
	private String dateFormatPattern;

	@Autowired
	private ReplacementStringsService replacementService;

	public Optional<TextConfiguration> getTextblockConfiguration(Long statementId)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}

		TblStatement statement = oStatement.get();
		TblWorkflowdata wfd = statement.getWorkflowdata();
		if (wfd == null) {
			throw new NotFoundServiceException("Workflowdata is not set.");
		}

		TblTextblockdefinition tbd = wfd.getTextBlockDefinition();
		if (tbd == null) {
			throw new InternalErrorServiceException("Error in statement. No TextblockDefinition assigned.");
		}

		TextConfiguration textConfiguration = new TextConfiguration();
		textConfiguration.setConfiguration(tbd.getDefinition());

		textConfiguration.setReplacements(replacementService.getAllReplacements(statement));

		return Optional.of(textConfiguration);
	}

	public Optional<List<Textblock>> getTextArrangement(Long statementId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}
		TblStatement statement = oStatement.get();

		Optional<TblStatementdraft> oLatestStatementDraft = statementDraftRepository
				.findFirstByWorkflowIdOrderByIdDesc(statement.getWorkflowdata().getId());
		if (oLatestStatementDraft.isPresent()) {
			return Optional.of(oLatestStatementDraft.get().getDraft());
		}
		return Optional.of(new ArrayList<>());
	}

	protected void verifyTextArrangement(List<Textblock> blocks, Long statementId) throws BadRequestServiceException,
			NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		if (blocks == null) {
			throw new BadRequestServiceException("Invalid text arrangement format. Provided text arrangement is null.");
		}
		Optional<TextConfiguration> oTextblockConfiguration = getTextblockConfiguration(statementId);
		if (oTextblockConfiguration.isEmpty()) {
			throw new NotFoundServiceException("Statement could not be found");
		}
		TextblockDefinition textblockDefinition = oTextblockConfiguration.get().getConfiguration();
		for (int i = 0; i < blocks.size(); i++) {
			Textblock block = blocks.get(i);
			if (block == null) {
				throw new BadRequestServiceException(INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK + i + " is null.");
			}
			if (block.getType() == null) {
				throw new BadRequestServiceException(
						INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK + i + " has invalid type parameter");
			}
			if (block.getType() == TextblockType.block && TextCompileUtil
					.getTextblockDefForTextblockId(textblockDefinition, block.getTextblockId()) == null) {
				throw new BadRequestServiceException(
						INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK + i + " does not have a valid textblockId");
			}
			if (block.getType() == TextblockType.text && block.getReplacement() == null) {
				throw new BadRequestServiceException(INVALID_TEXT_ARRANGEMENT_FORMAT_TEXT_BLOCK + i
						+ " of type text does not have a replacement text");
			}
		}

	}

	public void setTextArrangement(Long statementId, String taskId, List<Textblock> textArrangement)
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException,
			NotFoundServiceException {
		Optional<TaskInfo> ti = statementProcessService.getTaskInfo(statementId, taskId);
		if (ti.isPresent()) {
			authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
			String constraint = usersService.userOfRequiredDivisionsOfStatement(statementId,
					ti.get().getAssignee()) == null ? AuthorizationRuleActions.C_IS_CLAIMED_BY_USER
							: AuthorizationRuleActions.C_IS_REQ_DIVISION_MEMBER;

			authorizationService.authorize(ti.get().getTaskDefinitionKey(),
					AuthorizationRuleActions.A_SET_TEXTARRANGEMENT, constraint);
		}

		verifyTextArrangement(textArrangement, statementId);

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException(STATEMENT_WITH_GIVEN_IDENTIFIER_COULD_NOT_BE_FOUND);
		}
		TblStatement statement = oStatement.get();
		TblWorkflowdata wfd = statement.getWorkflowdata();
		if (wfd == null) {
			throw new NotFoundServiceException("Workflowdata is not set.");
		}

		Optional<TblStatementdraft> oLatestStatementDraft = statementDraftRepository
				.findFirstByWorkflowIdOrderByIdDesc(wfd.getId());

		if (oLatestStatementDraft.isPresent()) {
			TblStatementdraft latestStatementDraft = oLatestStatementDraft.get();
			if (StatementDraftUtils.equalIgnoreBothNull(latestStatementDraft.getDraft(), textArrangement)) {
				// same textblock already persisted
				return;
			}
		}

		StatementDraftUtils.fillEmptyTextblockUids(textArrangement);

		TblStatementdraft newStatementDraft = new TblStatementdraft();
		newStatementDraft.setDraft(textArrangement);
		newStatementDraft.setWorkflowId(wfd.getId());
		usersService.getOwnUserId().ifPresent(newStatementDraft::setUserId);
		String msg = "wf-task:" + ti.map(TaskInfo::getTaskDefinitionKey).orElse("unknown");
		newStatementDraft.setMsg(msg);
		newStatementDraft.setTimestamp(Time.currentTimeLocalTimeUTC());

		statementDraftRepository.save(newStatementDraft);

		statementProcessService.touchNewUserOperation(statementId, taskId);
	}

	public ValidationResult validate(Long statementId, List<Textblock> textArrangement)
			throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException(STATEMENT_WITH_GIVEN_IDENTIFIER_COULD_NOT_BE_FOUND);
		}
		Optional<TextConfiguration> oTextblockConfiguration = getTextblockConfiguration(statementId);
		if (oTextblockConfiguration.isEmpty()) {
			throw new InternalErrorServiceException("Could not get text block configuration");
		}

		TextConfiguration textConfiguration = oTextblockConfiguration.get();
		ValidationResult result = new ValidationResult();
		List<ValidationError> errors = new ArrayList<>();
		for (int i = 0; i < textArrangement.size(); i++) {
			Textblock block = textArrangement.get(i);
			ValidationError validationError = new ValidationError();
			validationError.setArrangementId(i);
			String text;
			switch (block.getType()) {
			case text:
				text = TextCompileUtil.fillPlaceholder(block.getReplacement(), block.getPlaceholderValues(),
						textConfiguration.getReplacements(), textConfiguration.getConfiguration().getSelects());
				validationError.setMissingVariables(TextCompileUtil.getPlaceholder(text));
				validationError.setRequires(new ArrayList<>());
				validationError.setExcludes(new ArrayList<>());
				break;
			case block:
				validationError.setTextBlockId(block.getTextblockId());
				validationError.setTextBlockGroup(TextCompileUtil
						.textBlockGroupForId(block.getTextblockId(), textConfiguration.getConfiguration())
						.getGroupName());
				if (block.getReplacement() == null) {
					text = TextCompileUtil.convertToText(block, textConfiguration);
				} else {
					text = TextCompileUtil.fillPlaceholder(block.getReplacement(), block.getPlaceholderValues(),
							textConfiguration.getReplacements(), textConfiguration.getConfiguration().getSelects());
				}
				validationError.setMissingVariables(TextCompileUtil.getPlaceholder(text));
				validationError.setAfter(TextCompileUtil.getAfter(textArrangement, i, textConfiguration));
				validationError.setRequires(
						TextCompileUtil.getRequires(textArrangement, block, textConfiguration.getConfiguration()));
				validationError.setExcludes(
						TextCompileUtil.getExcludes(textArrangement, block, textConfiguration.getConfiguration()));
				break;
			case newline:
			case pagebreak:
			default:
				validationError.setMissingVariables(new ArrayList<>());
				validationError.setRequires(new ArrayList<>());
				validationError.setExcludes(new ArrayList<>());
				break;
			}
			if (!isValid(validationError)) {
				errors.add(validationError);
			}
		}
		result.setErrors(errors);
		result.setValid(errors.isEmpty());
		return result;
	}

	private boolean isValid(ValidationError validationError) {
		if (validationError.getAfter() != null) {
			return false;
		}
		if (!validationError.getExcludes().isEmpty()) {
			return false;
		}
		if (!validationError.getMissingVariables().isEmpty()) {
			return false;
		}
		return validationError.getRequires().isEmpty();
	}

	public AttachmentFile compileTextArrangement(Long statementId, List<Textblock> textArrangement)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException,
			BadRequestServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TextConfiguration> oTextblockConfiguration = getTextblockConfiguration(statementId);
		if (oTextblockConfiguration.isEmpty()) {
			throw new NotFoundServiceException(STATEMENT_WITH_GIVEN_IDENTIFIER_COULD_NOT_BE_FOUND);
		}
		Optional<CompanyContactBlockModel> oContact = statementService.getContactBlock(statementId);
		if (oContact.isEmpty()) {
			throw new InternalErrorServiceException("Could not get contact details");
		}
		CompanyContactBlockModel contact = oContact.get();

		return statementCompileService.generatePDF(oTextblockConfiguration.get(), contact, textArrangement);
	}

}
