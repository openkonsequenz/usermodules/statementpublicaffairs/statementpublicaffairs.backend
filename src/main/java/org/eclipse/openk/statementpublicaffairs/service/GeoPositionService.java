/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.openk.statementpublicaffairs.model.Pair;
import org.eclipse.openk.statementpublicaffairs.model.Position;
import org.locationtech.proj4j.BasicCoordinateTransform;
import org.locationtech.proj4j.CRSFactory;
import org.locationtech.proj4j.ProjCoordinate;
import org.springframework.stereotype.Service;

/**
 * Service for geo postition transformations.
 * 
 * @author Tobias Stummer
 *
 */
@Service
public class GeoPositionService {

	private CRSFactory factory = new CRSFactory();

	private Map<Pair<String, String>, BasicCoordinateTransform> transformMap = new HashMap<>();

	private synchronized BasicCoordinateTransform getCoordinateTransform(String from, String to) {
		Pair<String, String> transformKey = new Pair<>();
		transformKey.setA(from);
		transformKey.setB(to);
		return transformMap.computeIfAbsent(transformKey,
				k -> new BasicCoordinateTransform(factory.createFromName(from), factory.createFromName(to)));
	}

	/**
	 * Transform the positions of the positions map according to the transformation
	 * coordinate parameters. typical transformation format is
	 * 
	 * @param positions map containing position entries to transform by identifier
	 *                  key
	 * @param from      geo format of the positions to transform from
	 * @param to        geo format to transform the positions to
	 * @return map containing the transformed positions by identifier key
	 */
	public synchronized Map<String, Position> transform(Map<String, Position> positions, String from, String to) {

		BasicCoordinateTransform transdform = getCoordinateTransform(from, to);

		Map<String, Position> response = new HashMap<>();
		if (transdform == null) {
			return response;
		}

		for (Entry<String, Position> e : positions.entrySet()) {
			Position p = e.getValue();
			ProjCoordinate src = new ProjCoordinate(p.getX(), p.getY());
			ProjCoordinate dst = new ProjCoordinate();
			transdform.transform(src, dst);
			Position tp = new Position();
			tp.setX(dst.x);
			tp.setY(dst.y);
			response.put(e.getKey(), tp);
		}
		return response;

	}

}
