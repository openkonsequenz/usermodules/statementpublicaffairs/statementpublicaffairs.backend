/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.UserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * UserInfoService provides easy access to the user details of the requesting
 * user.
 * 
 * @author Tobias Stummer
 *
 */
@Service
public class UserInfoService {

	/**
	 * Getter for user roles of current user.
	 * 
	 * @return Array of user roles.
	 */
	public String[] getUserRoles() {
		Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		List<String> userRoles = new ArrayList<>();
		for (GrantedAuthority authority : authorities) {
			userRoles.add(authority.getAuthority());
		}
		return userRoles.toArray(new String[0]);
	}

	/**
	 * Getter for first name of current user.
	 * 
	 * @return First name
	 */
	public String getFirstName() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userDetails.getFirstName();
	}

	/**
	 * Getter for last name of current user.
	 * 
	 * @return Last name
	 */
	public String getLastName() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userDetails.getLastName();
	}

	/**
	 * Getter for the username.
	 * 
	 * @return username
	 */
	public String getUserName() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userDetails.getUserName();
	}

	/**
	 * Getter for the access token.
	 * 
	 * @return access token
	 */
	public String getToken() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userDetails.getToken();
	}

	public Boolean isOwnUserName(String username) {
		if (username == null) {
			return false;
		}
		return (username.contentEquals(getUserName()));
	}


}
