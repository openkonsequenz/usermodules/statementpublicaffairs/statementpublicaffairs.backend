/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

/**
 * @author Tobias Stummer
 *
 */
class TypeConversionTest {

	@Test
	void testDateConversion() {

		String dateString = "2020-04-22";
		LocalDate date = TypeConversion.dateOfDateString(dateString).get();
		String convertedDateString = TypeConversion.dateStringOfLocalDate(date).get();
		assertEquals(dateString, convertedDateString);

	}

	@Test
	void testDateConversionInvalidShouldReturnOptionalEmpty() {
		assertFalse(TypeConversion.dateOfDateString(null).isPresent());
		assertFalse(TypeConversion.dateStringOfLocalDate(null).isPresent());
		assertFalse(TypeConversion.dateOfDateString("invalid").isPresent());
	}

	@Test
	void testStringOfBusinessKeyWithNullShouldReturnOptionalEmpty() {
		assertFalse(TypeConversion.stringOfBusinessKey(null).isPresent());
	}

	@Test
	void testBusinessKeyOfStringInvalidShouldReturnOptionalEmpty() {
		assertFalse(TypeConversion.businessKeyOfString(null).isPresent());
		assertFalse(TypeConversion.businessKeyOfString("invalid").isPresent());

	}

	@Test
	void testzDTOfCamundaDateStringInvalidShouldReturnOptionalEmpty() {
		assertFalse(TypeConversion.zDTOfCamundaDateString(null).isPresent());
		assertFalse(TypeConversion.zDTOfCamundaDateString("invalid").isPresent());
		assertFalse(TypeConversion.zDTOfCamundaDateString("invalid").isPresent());
		assertFalse(TypeConversion.camundaDateStringOfZDT(null).isPresent());
	}

	@Test
	void testiso8601InstantStringOfZonedDateTimeInvalidShouldReturnOptionalEmpty() {
		assertFalse(TypeConversion.iso8601InstantStringOfZonedDateTime(null).isPresent());

		assertFalse(TypeConversion.zonedDateTimeOfIso8601InstantString(null).isPresent());
		assertFalse(TypeConversion.zonedDateTimeOfIso8601InstantString("invalid").isPresent());

		assertFalse(TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(null).isPresent());
		assertFalse(TypeConversion.localDateTimeUTCofIso8601InstantString(null).isPresent());
		assertFalse(TypeConversion.localDateTimeUTCofIso8601InstantString("invalid").isPresent());
	}

	@Test
	void testCamundaDateTimeConversion() {
		String dateTimeString = "2020-05-05T16:02:39.271+0000";
		ZonedDateTime zdt = TypeConversion.zDTOfCamundaDateString(dateTimeString).get();
		String convertedDateTimeString = TypeConversion.camundaDateStringOfZDT(zdt).get();
		assertEquals(dateTimeString, convertedDateTimeString);
	}

	@Test
	void testIso8601InstanceStringOfLocalDateTimeUTC() {
		String iso8601InstanteString = "2011-12-03T10:15:30Z";
		LocalDateTime dateTime = TypeConversion.localDateTimeUTCofIso8601InstantString(iso8601InstanteString).get();
		String convertedIso8601InstanceString = TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(dateTime).get();
		assertEquals(iso8601InstanteString, convertedIso8601InstanceString);
	}

	@Test
	void testIso8601InstanceStringOfZonedDateTime() {
		String iso8601InstanteString = "2011-12-03T10:15:30Z";
		ZonedDateTime dateTime = TypeConversion.zonedDateTimeOfIso8601InstantString(iso8601InstanteString).get();
		String convertedIso8601InstanceString = TypeConversion.iso8601InstantStringOfZonedDateTime(dateTime).get();
		assertEquals(iso8601InstanteString, convertedIso8601InstanceString);
	}

}
