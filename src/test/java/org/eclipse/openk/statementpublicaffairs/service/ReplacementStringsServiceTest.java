/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationReplacementStringsService;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartmentstructure;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementtype;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessActivityHistoryViewModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessHistoryModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = TestConfigurationReplacementStringsService.class)
class ReplacementStringsServiceTest {

	private static final String REPLACEMENT_KEY_CONTACT_C_GREETING = "c-greeting";

	private static final String REPLACEMENT_KEY_CONTACT_C_TITLE = "c-title";

	private static final String REPLACEMENT_KEY_CONTACT_C_STREET = "c-street";

	private static final String REPLACEMENT_KEY_CONTACT_C_SALUTATION = "c-salutation";

	private static final String REPLACEMENT_KEY_CONTACT_C_POST_CODE = "c-postCode";

	private static final String REPLACEMENT_KEY_CONTACT_C_LAST_NAME = "c-lastName";

	private static final String REPLACEMENT_KEY_CONTACT_C_HOUSE_NUMBER = "c-houseNumber";

	private static final String REPLACEMENT_KEY_CONTACT_C_FIRST_NAME = "c-firstName";

	private static final String REPLACEMENT_KEY_CONTACT_C_EMAIL = "c-email";

	private static final String REPLACEMENT_KEY_CONTACT_C_COMPANY = "c-company";

	private static final String REPLACEMENT_KEY_CONTACT_C_COMMUNITY_SUFFIX = "c-communitySuffix";

	private static final String REPLACEMENT_KEY_CONTACT_C_COMMUNITY = "c-community";

	private static final String REPLACEMENT_KEY_CURRENT_DATE = "b-currentDate";

	private static final String REPLACEMENT_KEY_STATEMENT_SACHBEARBEITER = "sachbearbeiter";

	private static final String REPLACEMENT_KEY_STATEMENT_SECTORS = "sectors";

	private static final String REPLACEMENT_KEY_STATEMENT_LOCATION_DESIGNATION = "locationDesignation";

	private static final String REPLACEMENT_KEY_STATEMENT_CITY_WITH_LOCATION_DESIGNATION = "cityWithLocationDesignation";

	private static final String REPLACEMENT_KEY_STATEMENT_TYPE = "type";

	private static final String REPLACEMENT_KEY_STATEMENT_DISTRICT = "district";

	private static final String REPLACEMENT_KEY_STATEMENT_CITY = "city";

	private static final String REPLACEMENT_KEY_STATEMENT_CUSTOMER_REFERENCE = "customerReference";

	private static final String REPLACEMENT_KEY_STATEMENT_CREATION_DATE = "creationDate";

	private static final String REPLACEMENT_KEY_STATEMENT_RECEIPT_DATE = "receiptDate";

	private static final String REPLACEMENT_KEY_STATEMENT_DUE_DATE = "dueDate";

	private static final String REPLACEMENT_KEY_STATEMENT_DEPARTMENTS_DUE_DATE = "departmentsDueDate";

	private static final String REPLACEMENT_KEY_STATEMENT_TITLE = "title";

	private static final String REPLACEMENT_KEY_STATEMENT_ID = "id";

	// private static final String NOT_FOUND_VALUE = "not found value";

	@Autowired
	private ReplacementStringsService service;

	@Autowired
	private StatementService statementService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private ContactService contactService;

	@Autowired
	private DepartmentstructureRepository departmentStructureRepository;

	@Test
	void testAllReplacementStringsAreCreated()
			throws NotFoundServiceException, ForbiddenServiceException, InternalErrorServiceException {

		TblStatement statement = Mockito.mock(TblStatement.class);

		// statementReplacements
		Long statementId = 123L;
		Long departmentStructureId = 34L;
		String city = "city";
		String district = "District";
		String customerReference = "customerReference";
		String title = "title";
		LocalDate receiptDate = LocalDate.now().minusDays(3);
		String receiptDateString = TypeConversion.dateStringOfLocalDate(receiptDate, "dd.MM.yyyy").get();
		LocalDate creationDate = LocalDate.now().minusDays(2);
		String creationDateString = TypeConversion.dateStringOfLocalDate(creationDate, "dd.MM.yyyy").get();
		LocalDate dueDate = LocalDate.now().plusDays(2);
		String dueDateString = TypeConversion.dateStringOfLocalDate(dueDate, "dd.MM.yyyy").get();
		LocalDate departmentsDueDate = LocalDate.now().plusDays(1);
		String departmentsDueDateString = TypeConversion.dateStringOfLocalDate(departmentsDueDate, "dd.MM.yyyy").get();
		String statementType = "statementType";

		String cGreeting = "Sehr geehrte(r)";
		String cTitle = "cTitle";
		String cStreet = "cStreet";
		String cSalutation = "cSalutation";
		String cPostCode = "cPostCode";
		String cLastName = "cLastName";
		String cHouseNumber = "cHouseNumber";
		String cFirstName = "cFirstName";
		String cEmail = "cEmail";
		String cCompany = "cCompany";
		String cCommunitySuffix = "cCommunitySuffix";
		String cCommunity = "cCommunity";
		String bCurrentDate = TypeConversion.dateStringOfLocalDate(LocalDate.now(), "dd.MM.yyyy").get();

		String sector1 = "Sector1";
		String sector2 = "Sector2";
		String sector3 = "Sector3";
		String sectors = "Sector1, Sector2 und Sector3";
		String locationDesignation = "Gemeinde";
		String cityWithLocationDesignation = locationDesignation + " " + city;

		String contactDbId = "contactDbId";

		Mockito.when(statement.getId()).thenReturn(statementId);
		Mockito.when(statement.getCity()).thenReturn(city);
		Mockito.when(statement.getDistrict()).thenReturn(district);
		Mockito.when(statement.getCustomerReference()).thenReturn(customerReference);
		Mockito.when(statement.getTitle()).thenReturn(title);

		Mockito.when(statement.getCreationDate()).thenReturn(creationDate);
		Mockito.when(statement.getReceiptDate()).thenReturn(receiptDate);
		Mockito.when(statement.getDueDate()).thenReturn(dueDate);
		Mockito.when(statement.getDepartmentsDueDate()).thenReturn(departmentsDueDate);

		Mockito.when(statement.getContactDbId()).thenReturn(contactDbId);

		TblStatementtype type = Mockito.mock(TblStatementtype.class);
		Mockito.when(type.getName()).thenReturn(statementType);
		Mockito.when(statement.getType()).thenReturn(type);

		TblDepartmentstructure departmentStructure = Mockito.mock(TblDepartmentstructure.class);
		Map<String, DistrictDepartmentsModel> departmentStructureDefinition = new HashMap<>();
		String sectorKey = city + "#" + district;
		DistrictDepartmentsModel districtDepartmentsModel = Mockito.mock(DistrictDepartmentsModel.class);
		Mockito.when(districtDepartmentsModel.getLocationDesignation()).thenReturn(locationDesignation);

		departmentStructureDefinition.put(sectorKey, districtDepartmentsModel);
		Mockito.when(departmentStructure.getDefinition()).thenReturn(departmentStructureDefinition);
		Mockito.when(statement.getDepartmentStructureId()).thenReturn(departmentStructureId);
		Mockito.when(departmentStructureRepository.findById(departmentStructureId))
				.thenReturn(Optional.of(departmentStructure));

		// workflowReplacements

		List<UserModel> oicUsers = new ArrayList<>();
		UserModel um = new UserModel();
		String oicUser = "oicUser";
		String oicUserFn = "User";
		String oicUserLn = "Name";
		String oicFullName = oicUserFn + " " + oicUserLn;
		um.setUsername(oicUser);
		um.setFirstName(oicUserFn);
		um.setLastName(oicUserLn);
		oicUsers.add(um);
		Mockito.when(usersService.getUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE)).thenReturn(oicUsers);

		ProcessHistoryModel history = Mockito.mock(ProcessHistoryModel.class);

		Mockito.when(history.getCurrentProcessActivities()).thenReturn(new ArrayList<>());
		ProcessActivityHistoryViewModel act = new ProcessActivityHistoryViewModel();
		act.setAssignee(oicUser);
		List<ProcessActivityHistoryViewModel> finishedActivities = new ArrayList<>();
		finishedActivities.add(act);
		Mockito.when(history.getFinishedProcessActivities()).thenReturn(finishedActivities);
		Mockito.when(statementProcessService.getStatementProcessHistoryInternal(statementId)).thenReturn(Optional.of(history));

		// sectorsReplacements
		Map<String, List<String>> allSectors = new HashMap<>();
		List<String> sectorList = new ArrayList<>();
		sectorList.add(sector1);
		sectorList.add(sector2);
		sectorList.add(sector3);
		allSectors.put(sectorKey, sectorList);
		Mockito.when(statementService.getAllSectorsInternal(statementId)).thenReturn(allSectors);

		// contactReplacements

		CompanyContactBlockModel contact = Mockito.mock(CompanyContactBlockModel.class);
		Mockito.when(contact.getCommunity()).thenReturn(cCommunity);
		Mockito.when(contact.getCommunitySuffix()).thenReturn(cCommunitySuffix);
		Mockito.when(contact.getCompany()).thenReturn(cCompany);
		Mockito.when(contact.getEmail()).thenReturn(cEmail);
		Mockito.when(contact.getFirstName()).thenReturn(cFirstName);
		Mockito.when(contact.getHouseNumber()).thenReturn(cHouseNumber);
		Mockito.when(contact.getLastName()).thenReturn(cLastName);
		Mockito.when(contact.getPostCode()).thenReturn(cPostCode);
		Mockito.when(contact.getSalutation()).thenReturn(cSalutation);
		Mockito.when(contact.getStreet()).thenReturn(cStreet);
		Mockito.when(contact.getTitle()).thenReturn(cTitle);
		Mockito.when(contact.getSalutation()).thenReturn(cSalutation);
		Mockito.when(contactService.getContactDetails(contactDbId, true)).thenReturn(Optional.of(contact));

		Map<String, String> allReplacements = service.getAllReplacements(statement);
		
		Map<String, String> compare = new HashMap<>();
		compare.put(REPLACEMENT_KEY_CURRENT_DATE, bCurrentDate);
		compare.put(REPLACEMENT_KEY_CONTACT_C_COMMUNITY, cCommunity);
		compare.put(REPLACEMENT_KEY_CONTACT_C_COMMUNITY_SUFFIX, cCommunitySuffix);
		compare.put(REPLACEMENT_KEY_CONTACT_C_COMPANY, cCompany);
		compare.put(REPLACEMENT_KEY_CONTACT_C_EMAIL, cEmail);
		compare.put(REPLACEMENT_KEY_CONTACT_C_FIRST_NAME, cFirstName);
		compare.put(REPLACEMENT_KEY_CONTACT_C_GREETING, cGreeting);
		compare.put(REPLACEMENT_KEY_CONTACT_C_HOUSE_NUMBER, cHouseNumber);
		compare.put(REPLACEMENT_KEY_CONTACT_C_LAST_NAME, cLastName);
		compare.put(REPLACEMENT_KEY_CONTACT_C_POST_CODE, cPostCode);
		compare.put(REPLACEMENT_KEY_CONTACT_C_SALUTATION, cSalutation);
		compare.put(REPLACEMENT_KEY_CONTACT_C_STREET, cStreet);
		compare.put(REPLACEMENT_KEY_CONTACT_C_TITLE, cTitle);
		compare.put(REPLACEMENT_KEY_STATEMENT_CITY, city);
		compare.put(REPLACEMENT_KEY_STATEMENT_CITY_WITH_LOCATION_DESIGNATION, cityWithLocationDesignation);
		compare.put(REPLACEMENT_KEY_STATEMENT_CREATION_DATE, creationDateString);
		compare.put(REPLACEMENT_KEY_STATEMENT_CUSTOMER_REFERENCE, customerReference);
		compare.put(REPLACEMENT_KEY_STATEMENT_DEPARTMENTS_DUE_DATE, departmentsDueDateString);
		compare.put(REPLACEMENT_KEY_STATEMENT_DISTRICT, district);
		compare.put(REPLACEMENT_KEY_STATEMENT_DUE_DATE, dueDateString);
		compare.put(REPLACEMENT_KEY_STATEMENT_LOCATION_DESIGNATION, locationDesignation);
		compare.put(REPLACEMENT_KEY_STATEMENT_SACHBEARBEITER, oicFullName);
		compare.put(REPLACEMENT_KEY_STATEMENT_RECEIPT_DATE, receiptDateString);
		compare.put(REPLACEMENT_KEY_STATEMENT_SECTORS, sectors);
		compare.put(REPLACEMENT_KEY_STATEMENT_ID, statementId.toString());
		compare.put(REPLACEMENT_KEY_STATEMENT_TYPE, statementType);
		compare.put(REPLACEMENT_KEY_STATEMENT_TITLE, title);

		allReplacements.keySet().retainAll(compare.keySet());

		assertEquals(compare, allReplacements);

	}

}
