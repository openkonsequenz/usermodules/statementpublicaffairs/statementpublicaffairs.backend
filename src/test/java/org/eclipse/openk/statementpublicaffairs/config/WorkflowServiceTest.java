/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.api.CamundaApi;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationWorkflowService;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.ExternalTaskModel;
import org.eclipse.openk.statementpublicaffairs.model.ProcessActivityHistoryModel;
import org.eclipse.openk.statementpublicaffairs.model.ProcessDefinitionModel;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.WorkflowTaskModel;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaClaimTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaCompleteExternalTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaExternalTaskFetchAndLockRsp;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaFailureExternalTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaFetchAndLockExtTaskRequestBody;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinition;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionDiagramXML;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionStartReq;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessDefinitionStartRsp;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessHistoryStep;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaProcessInstance;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaTaskInstance;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaUserOperationLog;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaVariable;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaVariablesRequestBody;
import org.eclipse.openk.statementpublicaffairs.service.WorkflowService;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowTaskVariableModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import feign.FeignException;
import feign.Request;
import feign.Request.Body;
import feign.Response;

@SpringBootTest(classes = TestConfigurationWorkflowService.class)
@ActiveProfiles("test")

class WorkflowServiceTest {

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private CamundaApi camundaApi;

	@Value("${workflow.processDefinitionKey}")
	private String processDefinitionKey;

	@Value("${workflow.tenantId}")
	private String tenantId;

	@Test
	void getProcessDefinitionWithValidBusinessKeyShouldRespondWithProcessDefinition()
			throws InternalErrorServiceException {
		String businessKey = "businessKey";
		String definitionId = "definitionId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		processInstances.add(pi);

		String id = "id";
		String key = "key";
		String category = "category";
		String description = "description";
		String name = "name";
		Long version = 1L;
		String resource = "resource";
		String tenantId = "tenantId";
		String versionTag = "versionTag";

		CamundaProcessDefinition processDefinition = new CamundaProcessDefinition();
		processDefinition.setId(id);
		processDefinition.setKey(key);
		processDefinition.setCategory(category);
		processDefinition.setDescription(description);
		processDefinition.setName(name);
		processDefinition.setVersion(version);
		processDefinition.setResource(resource);
		processDefinition.setTenantId(tenantId);
		processDefinition.setVersionTag(versionTag);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);
		Mockito.when(camundaApi.getProcessDefinitionForId(Mockito.anyString(), Mockito.eq(definitionId)))
				.thenReturn(processDefinition);

		Optional<ProcessDefinitionModel> oPD = workflowService.getProcessDefinition(businessKey);

		assertTrue(oPD.isPresent());

		ProcessDefinitionModel pd = oPD.get();

		assertEquals(id, pd.getId());
		assertEquals(key, pd.getKey());
		assertEquals(category, pd.getCategory());
		assertEquals(description, pd.getDescription());
		assertEquals(name, pd.getName());
		assertEquals(version, pd.getVersion());
		assertEquals(resource, pd.getResource());
		assertEquals(tenantId, pd.getTenantId());
		assertEquals(versionTag, pd.getVersionTag());
	}

	@Test
	void getProcessDefinitionWithInvalidBusinessKeyShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException {
		String businessKey = "404";
		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(new ArrayList<CamundaProcessInstance>());

		assertFalse(workflowService.getProcessDefinition(businessKey).isPresent());
	}

	@Test
	void getProcessDefinitionWithValidBusinessKeyExceptionGettingProcessInstanceFromWorkflowEngineThrowsInternalErrorServiceException() {

		String businessKey = "businessKey";
		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi)
				.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(), Mockito.eq(businessKey));

		try {
			workflowService.getProcessDefinition(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getProcessDefinitionWithValidBusinessKeyExceptionGettingProcessDefinitionFromWorkflowEngineThrowsInternalErrorServiceException() {

		String businessKey = "businessKey";
		String definitionId = "definitionId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);
		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).getProcessDefinitionForId(Mockito.anyString(), Mockito.eq(definitionId));

		try {
			workflowService.getProcessDefinition(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void startProcessLatestVersionShouldRespondWithUUIDBasedBusinessKeyString() throws InternalErrorServiceException {

		String businessKey = "businessKey";
		CamundaProcessDefinitionStartRsp resp = new CamundaProcessDefinitionStartRsp();
		resp.setBusinessKey(businessKey);
		Mockito.when(camundaApi.startProcess(Mockito.eq(processDefinitionKey), Mockito.eq(tenantId),
				Mockito.anyString(), Mockito.any(CamundaProcessDefinitionStartReq.class))).thenReturn(resp);

		assertEquals(businessKey, workflowService.startProcessLatestVersion());
	}

	@Test
	void startProcessLatestVersionExceptionStartingProcessInstanceWithWorkflowEngineThrowsInternalErrorServiceException() {

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).startProcess(Mockito.eq(processDefinitionKey), Mockito.eq(tenantId),
						Mockito.anyString(), Mockito.any(CamundaProcessDefinitionStartReq.class));

		try {
			workflowService.startProcessLatestVersion();
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void getCurrentTaskOfProcessBusinessKeyWithValidBusinessKeyShouldRespondWithListOfCurrentWorkflowTaskModels()
			throws InternalErrorServiceException {

		String businessKey = "businessKey";
		String definitionId = "definitionId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		List<CamundaTaskInstance> cTasks = new ArrayList<>();
		CamundaTaskInstance task = new CamundaTaskInstance();

		String taskId = "taskId";
		String processInstanceId = "processInstanceId";
		String assignee = "assignee";
		String taskDefinitionKey = "taskDefinitionKey";
		Boolean suspended = false;
		String owner = "owner";
		String name = "name";
		String description = "description";
		String due = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		String created = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		String formKey = "formKey";
		task.setId(taskId);
		task.setProcessInstanceId(processInstanceId);
		task.setAssignee(assignee);
		task.setTenantId(tenantId);
		task.setTaskDefinitionKey(taskDefinitionKey);
		task.setSuspended(suspended);
		task.setOwner(owner);
		task.setName(name);
		task.setDescription(description);
		task.setDue(due);
		task.setCreated(created);
		task.setFormKey(formKey);
		cTasks.add(task);

		Map<String, CamundaVariable> rVariables = new HashMap<>();
		CamundaVariable var = new CamundaVariable();
		var.setLocal(false);
		var.setType("Boolean");
		rVariables.put("_var", var);

		Mockito.when(camundaApi.getCurrentTaskInstancesWithProcessInstanceBusinessKey(Mockito.anyString(),
				Mockito.eq(processDefinitionKey), Mockito.eq(businessKey))).thenReturn(cTasks);

		Mockito.when(camundaApi.getTaskInstanceFormVariables(Mockito.anyString(), Mockito.eq(taskId)))
				.thenReturn(rVariables);

		Optional<List<WorkflowTaskModel>> oTasks = workflowService.getCurrentTasksOfProcessBusinessKey(businessKey);

		assertTrue(oTasks.isPresent());
		List<WorkflowTaskModel> tasks = oTasks.get();
		WorkflowTaskModel wTask = tasks.get(0);
		assertEquals(taskId, wTask.getTaskId());
		assertEquals(processInstanceId, wTask.getProcessInstanceId());
		assertEquals(assignee, wTask.getAssignee());
		assertEquals(taskDefinitionKey, wTask.getTaskDefinitionKey());
		assertEquals(suspended, wTask.getSuspended());
		assertEquals(owner, wTask.getOwner());
		assertEquals(name, wTask.getName());
		assertEquals(description, wTask.getDescription());
		assertEquals(due, TypeConversion.camundaDateStringOfZDT(wTask.getDue()).get());
		assertEquals(created, TypeConversion.camundaDateStringOfZDT(wTask.getCreated()).get());
		assertEquals(formKey, wTask.getFormKey());
		assertEquals(processDefinitionKey, wTask.getProcessDefinitionKey());
		assertEquals(var.getType(), wTask.getRequiredVariables().get("var"));
	}

	@Test
	void getCurrentTaskOfProcessBusinessKeyWithInvalidBusinessKeyShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException {
		String businessKey = "businessKey";

		assertFalse(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey).isPresent());
	}

	@Test
	void getCurrentTaskOfProcessBusinessKeyWithValidBusinessKeyExceptionGettingProcessInstanceFromWorkflowEngineThrowsInternalErrorServiceException() {

		String businessKey = "businessKey";

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi)
				.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(), Mockito.eq(businessKey));

		try {
			workflowService.getCurrentTasksOfProcessBusinessKey(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getCurrentTaskOfProcessBusinessKeyWithValidBusinessKeyExceptionGettingCurrentTaskInstancesFromWorkflowEngineThrowsInternalErrorServiceException() {

		String businessKey = "businessKey";
		String definitionId = "definitionId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).getCurrentTaskInstancesWithProcessInstanceBusinessKey(Mockito.anyString(),
						Mockito.eq(processDefinitionKey), Mockito.eq(businessKey));

		try {
			workflowService.getCurrentTasksOfProcessBusinessKey(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getStatementProcessHistoryWithValidBusinessKeyShouldRespondWithListOfTaskHistoryEntries()
			throws InternalErrorServiceException {
		String businessKey = "businessKey";
		String definitionId = "definitionId";
		String processInstanceId = "processInstanceId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		pi.setId(processInstanceId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		List<CamundaProcessHistoryStep> historySteps = new ArrayList<>();

		CamundaProcessHistoryStep step = new CamundaProcessHistoryStep();

		String id = "id";
		String activityName = "activityName";
		String activityType = "activityType";
		String activityId = "activityId";
		String assignee = "assignee";
		String startTime = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		String endTime = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		Long durationInMillis = 1000L;
		String processDefinitionKey = "processDefinitionKey";
		String processDefinitionId = "processDefinitionId";
		Boolean canceled = false;
		Boolean completeScope = true;
		String tenantId = "tenantId";
		step.setId(id);
		step.setActivityName(activityName);
		step.setActivityType(activityType);
		step.setActivityId(activityId);
		step.setAssignee(assignee);
		step.setStartTime(startTime);
		step.setEndTime(endTime);
		step.setDurationInMillis(durationInMillis);
		step.setProcessDefinitionKey(processDefinitionKey);
		step.setProcessDefinitionId(processDefinitionId);
		step.setCanceled(canceled);
		step.setCompleteScope(completeScope);
		step.setTenantId(tenantId);
		historySteps.add(step);

		Mockito.when(camundaApi.getProcessHistory(Mockito.anyString(), Mockito.eq(processInstanceId)))
				.thenReturn(historySteps);

		Optional<List<ProcessActivityHistoryModel>> oHistory = workflowService.getStatementProcessHistory(businessKey);
		assertTrue(oHistory.isPresent());

		List<ProcessActivityHistoryModel> history = oHistory.get();

		ProcessActivityHistoryModel hstep = history.get(0);

		assertEquals(id, hstep.getId());
		assertEquals(activityName, hstep.getActivityName());
		assertEquals(activityId, hstep.getActivityId());
		assertEquals(assignee, hstep.getAssignee());
		assertEquals(startTime, TypeConversion.camundaDateStringOfZDT(hstep.getStartTime()).get());
		assertEquals(endTime, TypeConversion.camundaDateStringOfZDT(hstep.getEndTime()).get());
		assertEquals(durationInMillis, hstep.getDurationInMillis());
		assertEquals(processDefinitionKey, hstep.getProcessDefinitionKey());
		assertEquals(processDefinitionId, hstep.getProcessDefinitionId());
		assertEquals(canceled, hstep.getCanceled());
		assertEquals(completeScope, hstep.getCompleteScope());
		assertEquals(tenantId, hstep.getTenantId());
	}

	@Test
	void getStatementProcessHistoryWithInvalidBusinessKeyShouldRespondOptionalEmpty()
			throws InternalErrorServiceException {

		String businessKey = "404";

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(new ArrayList<CamundaProcessInstance>());
		assertFalse(workflowService.getStatementProcessHistory(businessKey).isPresent());

	}

	@Test
	void getStatementProcessHistoryWithValidBusinessKeyMoreThanOneProcessInstancesWithSameBusinessKeyShouldThrowInternalErrorServiceException() {

		String businessKey = "businessKey";
		String definitionId = "definitionId";
		String processInstanceId = "processInstanceId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		pi.setId(processInstanceId);
		processInstances.add(pi);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		try {
			workflowService.getStatementProcessHistory(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getStatementProcessHistoryWithValidBusinessKeyExceptionGettingProcessInstanceFromWorkflowEngineShouldThrowInternalErrorServiceException() {

		String businessKey = "404";

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi)
				.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(), Mockito.eq(businessKey));

		try {
			workflowService.getStatementProcessHistory(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getStatementProcessHistoryWithValidBusinessKeyExceptionGettingProcessHistoryFromWorkflowEngineShouldThrowInternalErrorServiceException() {

		String businessKey = "businessKey";
		String definitionId = "definitionId";
		String processInstanceId = "processInstanceId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		pi.setId(processInstanceId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).getProcessHistory(Mockito.anyString(), Mockito.eq(processInstanceId));

		try {
			workflowService.getStatementProcessHistory(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void claimTaskWithValidTaskIdAndAssigneeShouldRespondWorkflowDetails() throws InternalErrorServiceException {

		String assignee = "assignee";
		String taskId = "taskId";

		Response response = Response.builder().headers(Collections.emptyMap()).status(204).request(
				Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.claimTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaClaimTaskRequestBody.class))).thenReturn(response);

		String processInstanceId = "processInstanceId";
		String taskDefinitionKey = "taskDefinitionKey";
		Boolean suspended = false;
		String owner = "owner";
		String name = "name";
		String description = "description";
		String due = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		String created = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		String formKey = "formKey";

		CamundaTaskInstance cTask = new CamundaTaskInstance();
		cTask.setId(taskId);
		cTask.setProcessInstanceId(processInstanceId);
		cTask.setAssignee(assignee);
		cTask.setTenantId(tenantId);
		cTask.setTaskDefinitionKey(taskDefinitionKey);
		cTask.setSuspended(suspended);
		cTask.setOwner(owner);
		cTask.setName(name);
		cTask.setDescription(description);
		cTask.setDue(due);
		cTask.setCreated(created);
		cTask.setFormKey(formKey);

		Mockito.when(camundaApi.getTaskInstance(Mockito.anyString(), Mockito.eq(taskId))).thenReturn(cTask);

		Map<String, CamundaVariable> rVariables = new HashMap<>();
		CamundaVariable var = new CamundaVariable();
		var.setLocal(false);
		var.setType("Boolean");
		rVariables.put("_var", var);

		Mockito.when(camundaApi.getTaskInstanceFormVariables(Mockito.anyString(), Mockito.eq(taskId)))
				.thenReturn(rVariables);

		Optional<WorkflowTaskModel> oTask = workflowService.claimTask(taskId, assignee);
		assertTrue(oTask.isPresent());
		WorkflowTaskModel wTask = oTask.get();

		assertEquals(taskId, wTask.getTaskId());
		assertEquals(processInstanceId, wTask.getProcessInstanceId());
		assertEquals(processDefinitionKey, wTask.getProcessDefinitionKey());
		assertEquals(assignee, wTask.getAssignee());
		assertEquals(tenantId, wTask.getTenantId());
		assertEquals(taskDefinitionKey, wTask.getTaskDefinitionKey());
		assertEquals(suspended, wTask.getSuspended());
		assertEquals(owner, wTask.getOwner());
		assertEquals(name, wTask.getName());
		assertEquals(description, wTask.getDescription());
		assertEquals(due, TypeConversion.camundaDateStringOfZDT(wTask.getDue()).get());
		assertEquals(created, TypeConversion.camundaDateStringOfZDT(wTask.getCreated()).get());
		assertEquals(formKey, wTask.getFormKey());

		assertEquals(var.getType(), wTask.getRequiredVariables().get("var"));

	}

	@Test
	void claimTaskWithInvalidTaskIdShouldRespondOptionalEmpty() throws InternalErrorServiceException {

		String assignee = "assignee";
		String taskId = "taskId";

		Response response = Response.builder().headers(Collections.emptyMap()).status(500).request(
				Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.claimTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaClaimTaskRequestBody.class))).thenReturn(response);

		assertFalse(workflowService.claimTask(taskId, assignee).isPresent());

	}

	@Test
	void claimTaskWithValidTaskIdExceptionClaimingTaskInstanceInWorkflowEngineThrowsInternalErrorServiceException() {

		String assignee = "assignee";
		String taskId = "taskId";

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).claimTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
						Mockito.any(CamundaClaimTaskRequestBody.class));

		try {
			workflowService.claimTask(taskId, assignee);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void claimTaskWithValidTaskIdExceptionGettingTaskInstanceFromWorkflowEngineThrowsInternalErrorServiceException() {

		String assignee = "assignee";
		String taskId = "taskId";

		Response response = Response.builder().headers(Collections.emptyMap()).status(204).request(
				Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.claimTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaClaimTaskRequestBody.class))).thenReturn(response);

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).getTaskInstance(Mockito.anyString(), Mockito.eq(taskId));

		try {
			workflowService.claimTask(taskId, assignee);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void unClaimTaskWithValidTaskIdShouldRespondWorkflowDetails() throws InternalErrorServiceException {

		String taskId = "taskId";

		Response response = Response.builder().headers(Collections.emptyMap()).status(204).request(
				Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.unclaimTaskInstance(Mockito.anyString(), Mockito.eq(taskId))).thenReturn(response);

		String processInstanceId = "processInstanceId";
		String taskDefinitionKey = "taskDefinitionKey";
		Boolean suspended = false;
		String owner = "owner";
		String name = "name";
		String description = "description";
		String due = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		String created = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		String formKey = "formKey";

		CamundaTaskInstance cTask = new CamundaTaskInstance();
		cTask.setId(taskId);
		cTask.setProcessInstanceId(processInstanceId);
		cTask.setAssignee(null);
		cTask.setTenantId(tenantId);
		cTask.setTaskDefinitionKey(taskDefinitionKey);
		cTask.setSuspended(suspended);
		cTask.setOwner(owner);
		cTask.setName(name);
		cTask.setDescription(description);
		cTask.setDue(due);
		cTask.setCreated(created);
		cTask.setFormKey(formKey);

		Mockito.when(camundaApi.getTaskInstance(Mockito.anyString(), Mockito.eq(taskId))).thenReturn(cTask);

		Map<String, CamundaVariable> rVariables = new HashMap<>();
		CamundaVariable var = new CamundaVariable();
		var.setLocal(false);
		var.setType("Boolean");
		rVariables.put("_var", var);

		Mockito.when(camundaApi.getTaskInstanceFormVariables(Mockito.anyString(), Mockito.eq(taskId)))
				.thenReturn(rVariables);

		Optional<WorkflowTaskModel> oTask = workflowService.unClaimTask(taskId);
		assertTrue(oTask.isPresent());
		WorkflowTaskModel wTask = oTask.get();

		assertEquals(taskId, wTask.getTaskId());
		assertEquals(processInstanceId, wTask.getProcessInstanceId());
		assertEquals(processDefinitionKey, wTask.getProcessDefinitionKey());
		assertNull(wTask.getAssignee());
		assertEquals(tenantId, wTask.getTenantId());
		assertEquals(taskDefinitionKey, wTask.getTaskDefinitionKey());
		assertEquals(suspended, wTask.getSuspended());
		assertEquals(owner, wTask.getOwner());
		assertEquals(name, wTask.getName());
		assertEquals(description, wTask.getDescription());
		assertEquals(due, TypeConversion.camundaDateStringOfZDT(wTask.getDue()).get());
		assertEquals(created, TypeConversion.camundaDateStringOfZDT(wTask.getCreated()).get());
		assertEquals(formKey, wTask.getFormKey());

		assertEquals(var.getType(), wTask.getRequiredVariables().get("var"));

	}

	@Test
	void unClaimTaskWithInvalidTaskIdShouldRespondOptionalEmpty() throws InternalErrorServiceException {

		String taskId = "taskId";

		Response response = Response.builder().headers(Collections.emptyMap()).status(400).request(
				Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.unclaimTaskInstance(Mockito.anyString(), Mockito.eq(taskId))).thenReturn(response);

		assertFalse(workflowService.unClaimTask(taskId).isPresent());

	}

	@Test
	void unClaimTaskWithValidTaskIdExceptionUnclaimingTaskInstanceInWorkflowEngineThrowsInternalErrorServiceException() {

		String taskId = "taskId";

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).unclaimTaskInstance(Mockito.anyString(), Mockito.eq(taskId));

		try {
			workflowService.unClaimTask(taskId);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void unClaimTaskWithValidTaskIdExceptionGettingTaskInstanceFromWorkflowEngineThrowsInternalErrorServiceException() {

		String taskId = "taskId";

		Response response = Response.builder().headers(Collections.emptyMap()).status(204).request(
				Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.unclaimTaskInstance(Mockito.anyString(), Mockito.eq(taskId))).thenReturn(response);

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).getTaskInstance(Mockito.anyString(), Mockito.eq(taskId));

		try {
			workflowService.unClaimTask(taskId);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void completeTaskWithValidTaskIdAndCompleteVariablesShouldRespondWithTrue()
			throws InternalErrorServiceException, BadRequestServiceException {
		String taskId = "taskId";
		Long statementId = 1234L;
		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		WorkflowTaskVariableModel var = new WorkflowTaskVariableModel();
		var.setType("Boolean");
		var.setValue(true);
		variables.put("var", var);

		Response response = Response.builder().headers(Collections.emptyMap()).status(204).request(
				Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeTask(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaVariablesRequestBody.class))).thenReturn(response);

		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskId()).thenReturn(taskId);
		Optional<Boolean> oCompleted = workflowService.completeTask(statementId, taskInfo, variables);
		assertTrue(oCompleted.isPresent());
		assertTrue(oCompleted.get());

	}

	@Test
	void completetTaskWithValidTaskIdAndExceptionCompleteTaskInWorkflowEngineShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {
		String taskId = "taskId";
		Long statementId = 1234L;

		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		WorkflowTaskVariableModel var = new WorkflowTaskVariableModel();
		var.setType("Boolean");
		var.setValue(true);
		variables.put("var", var);

		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskId()).thenReturn(taskId);

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi)
				.completeTask(Mockito.anyString(), Mockito.eq(taskId), Mockito.any(CamundaVariablesRequestBody.class));

		try {
			workflowService.completeTask(statementId, taskInfo, variables);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void completeTaskWithValidTaskIdAndBadRequestResponseCompleteTaskInWorkflowEngineShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException {
		String taskId = "taskId";
		Long statementId = 1234L;

		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		WorkflowTaskVariableModel var = new WorkflowTaskVariableModel();
		var.setType("Boolean");
		var.setValue(true);
		variables.put("var", var);

		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskId()).thenReturn(taskId);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(400).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeTask(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaVariablesRequestBody.class))).thenReturn(response);

		try {
			workflowService.completeTask(statementId, taskInfo, variables);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void completeTaskWithValidTaskIdAndInternalServerErrorResponseCompleteTaskInWorkflowEngineShouldInternalServerErrorException()
			throws BadRequestServiceException {
		String taskId = "taskId";
		Long statementId = 1234L;

		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		WorkflowTaskVariableModel var = new WorkflowTaskVariableModel();
		var.setType("Boolean");
		var.setValue(true);
		variables.put("var", var);

		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskId()).thenReturn(taskId);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(500).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeTask(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaVariablesRequestBody.class))).thenReturn(response);

		try {
			workflowService.completeTask(statementId, taskInfo, variables);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void completeTaskWithValidTaskIdAndInvalidResponseCompleteTaskInWorkflowEngineShouldInternalServerErrorException()
			throws BadRequestServiceException {
		String taskId = "taskId";
		Long statementId = 1234L;

		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		WorkflowTaskVariableModel var = new WorkflowTaskVariableModel();
		var.setType("Boolean");
		var.setValue(true);
		variables.put("var", var);

		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskId()).thenReturn(taskId);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(404).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeTask(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaVariablesRequestBody.class))).thenReturn(response);

		try {
			workflowService.completeTask(statementId, taskInfo, variables);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void fetchAndLockExternalTasksWithValidSetOfTopicsAndValidWorkerIdShouldRespondWithListOfExternalTasks()
			throws InternalErrorServiceException {
		Set<String> topics = new HashSet<>();
		String topicName = "topic";
		topics.add(topicName);
		String workerId = "workerId";

		String activityId = "acrivityId";
		String activityInstanceId = "activityInstanceId";
		String errorDetails = "errorDetails";
		String errorMessage = "errorMessage";
		String executionId = "executionId";
		String id = "id";
		String lockExpirationTime = "lockExpirationTime";
		Long priority = 1L;
		String processDefinitionId = "processDefinitionId";
		String processInstanceId = "processInstanceId";
		Long retries = 2L;

		Map<String, CamundaVariable> variables = new HashMap<>();
		CamundaVariable var = new CamundaVariable();
		var.setType("Boolean");
		var.setValue(true);
		variables.put("var", var);
		List<CamundaExternalTaskFetchAndLockRsp> extTasks = new ArrayList<>();
		CamundaExternalTaskFetchAndLockRsp extTask = new CamundaExternalTaskFetchAndLockRsp();
		extTask.setActivityId(activityId);
		extTask.setActivityInstanceId(activityInstanceId);
		extTask.setErrorDetails(errorDetails);
		extTask.setErrorMessage(errorMessage);
		extTask.setExecutionId(executionId);
		extTask.setId(id);
		extTask.setLockExpirationTime(lockExpirationTime);
		extTask.setPriority(priority);
		extTask.setProcessDefinitionId(processDefinitionId);
		extTask.setProcessDefinitionKey(processDefinitionKey);
		extTask.setProcessInstanceId(processInstanceId);
		extTask.setRetries(retries);
		extTask.setTenantId(tenantId);
		extTask.setTopicName(topicName);
		extTask.setWorkerId(workerId);
		extTask.setVariables(variables);

		extTasks.add(extTask);

		Mockito.when(camundaApi.fetchAndLockExternalTaskInstances(Mockito.anyString(),
				Mockito.any(CamundaFetchAndLockExtTaskRequestBody.class))).thenReturn(extTasks);

		Optional<List<ExternalTaskModel>> oTasks = workflowService.fetchAndLockExternalTasks(topics, workerId);
		assertTrue(oTasks.isPresent());
		ExternalTaskModel task = oTasks.get().get(0);
		assertEquals(activityId, task.getActivityId());
		assertEquals(activityInstanceId, task.getActivityInstanceId());
		assertEquals(errorDetails, task.getErrorDetails());
		assertEquals(errorMessage, task.getErrorMessage());
		assertEquals(executionId, task.getExecutionId());
		assertEquals(id, task.getId());
		assertEquals(lockExpirationTime, task.getLockExpirationTime());
		assertEquals(priority, task.getPriority());
		assertEquals(processDefinitionId, task.getProcessDefinitionId());
		assertEquals(processDefinitionKey, task.getProcessDefinitionKey());
		assertEquals(processInstanceId, task.getProcessInstanceId());
		assertEquals(retries, task.getRetries());
		assertEquals(tenantId, task.getTenantId());
		assertEquals(topicName, task.getTopicName());
		assertEquals(workerId, task.getWorkerId());
		WorkflowTaskVariableModel wvar = task.getVariables().get("var");
		assertEquals(var.getType(), wvar.getType());
		assertEquals(var.getValue(), wvar.getValue());

	}

	@Test
	void fetchAndLockExternalTasksWithValidSetOfTopicsAndValidWorkerIdExceptionAccessingTheWorkflowEnfineShouldThrowInternalErrorServiceException() {
		Set<String> topics = new HashSet<>();
		String topicName = "topic";
		topics.add(topicName);
		String workerId = "workerId";

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).fetchAndLockExternalTaskInstances(Mockito.anyString(),
						Mockito.any(CamundaFetchAndLockExtTaskRequestBody.class));

		try {
			workflowService.fetchAndLockExternalTasks(topics, workerId);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void completeExternalTaskWithValidTaskIdAndValidWorkerIdAndCompleteVariablesShouldRespondTrue()
			throws InternalErrorServiceException, BadRequestServiceException {

		String taskId = "taskId";
		String workerId = "workerId";
		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(204).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaCompleteExternalTaskRequestBody.class))).thenReturn(response);

		Optional<Boolean> oResponse = workflowService.completeExternalTask(taskId, workerId, variables);
		assertTrue(oResponse.isPresent());
		assertTrue(oResponse.get());

	}

	@Test
	void completeExternalTasksWithValidTaskIdAndValidWorkerIdExceptionAccessingTheWorkflowEngineShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {
		String taskId = "taskId";
		String workerId = "workerId";
		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).completeExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
						Mockito.any(CamundaCompleteExternalTaskRequestBody.class));

		try {
			workflowService.completeExternalTask(taskId, workerId, variables);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void completeExternalTaskWithInValidTaskIdOfShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException {

		String taskId = "taskId";
		String workerId = "workerId";
		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		CamundaCompleteExternalTaskRequestBody reqBody = new CamundaCompleteExternalTaskRequestBody();
		reqBody.setWorkerId(workerId);
		Map<String, CamundaVariable> camundaVariables = new HashMap<>();
		for (Entry<String, WorkflowTaskVariableModel> entry : variables.entrySet()) {
			WorkflowTaskVariableModel var = entry.getValue();
			CamundaVariable cv = new CamundaVariable();
			cv.setLocal(true);
			cv.setType(var.getType());
			cv.setValue(var.getValue());
		}
		reqBody.setVariables(camundaVariables);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(404).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaCompleteExternalTaskRequestBody.class))).thenReturn(response);

		try {
			workflowService.completeExternalTask(taskId, workerId, variables);
			fail("Should have thrown an BadRequestServiceException.");
		} catch (BadRequestServiceException e) {
			// pass
		}
	}

	@Test
	void completeExternalTaskWithValidTaskIdAndInvalidWorkerIdShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException {

		String taskId = "taskId";
		String workerId = "workerId";
		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		CamundaCompleteExternalTaskRequestBody reqBody = new CamundaCompleteExternalTaskRequestBody();
		reqBody.setWorkerId(workerId);
		Map<String, CamundaVariable> camundaVariables = new HashMap<>();
		for (Entry<String, WorkflowTaskVariableModel> entry : variables.entrySet()) {
			WorkflowTaskVariableModel var = entry.getValue();
			CamundaVariable cv = new CamundaVariable();
			cv.setLocal(true);
			cv.setType(var.getType());
			cv.setValue(var.getValue());
		}
		reqBody.setVariables(camundaVariables);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(400).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaCompleteExternalTaskRequestBody.class))).thenReturn(response);

		try {
			workflowService.completeExternalTask(taskId, workerId, variables);
			fail("Should have thrown an BadRequestServiceException.");
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void completeExternalTaskWithValidTaskIdAndValidWorkerIdInternalServerErrorResponseFromWorkflowEngineShouldRespondWithFalse()
			throws InternalErrorServiceException, BadRequestServiceException {
		String taskId = "taskId";
		String workerId = "workerId";
		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		CamundaCompleteExternalTaskRequestBody reqBody = new CamundaCompleteExternalTaskRequestBody();
		reqBody.setWorkerId(workerId);
		Map<String, CamundaVariable> camundaVariables = new HashMap<>();
		for (Entry<String, WorkflowTaskVariableModel> entry : variables.entrySet()) {
			WorkflowTaskVariableModel var = entry.getValue();
			CamundaVariable cv = new CamundaVariable();
			cv.setLocal(true);
			cv.setType(var.getType());
			cv.setValue(var.getValue());
		}
		reqBody.setVariables(camundaVariables);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(500).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaCompleteExternalTaskRequestBody.class))).thenReturn(response);

		Optional<Boolean> oResponse = workflowService.completeExternalTask(taskId, workerId, variables);
		assertTrue(oResponse.isPresent());
		assertFalse(oResponse.get());

	}

	@Test
	void completeExternalTaskWithValidTaskIdAndValidWorkerIdInvalidResponseFromWorkflowEngineShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {

		String taskId = "taskId";
		String workerId = "workerId";
		Map<String, WorkflowTaskVariableModel> variables = new HashMap<>();

		CamundaCompleteExternalTaskRequestBody reqBody = new CamundaCompleteExternalTaskRequestBody();
		reqBody.setWorkerId(workerId);
		Map<String, CamundaVariable> camundaVariables = new HashMap<>();
		for (Entry<String, WorkflowTaskVariableModel> entry : variables.entrySet()) {
			WorkflowTaskVariableModel var = entry.getValue();
			CamundaVariable cv = new CamundaVariable();
			cv.setLocal(true);
			cv.setType(var.getType());
			cv.setValue(var.getValue());
		}
		reqBody.setVariables(camundaVariables);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(406).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.completeExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaCompleteExternalTaskRequestBody.class))).thenReturn(response);

		try {
			workflowService.completeExternalTask(taskId, workerId, variables);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void failureExternalTaskWithValidTaskIdAndValidWorkerIdShouldRespondWithTrue()
			throws InternalErrorServiceException, BadRequestServiceException {

		String taskId = "taskId";
		String workerId = "workerId";
		String errorMessage = "errorMessage";

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(204).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.failureExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaFailureExternalTaskRequestBody.class))).thenReturn(response);
		Optional<Boolean> oResponse = workflowService.failureExternalTask(taskId, workerId, errorMessage);
		assertTrue(oResponse.isPresent());
		assertTrue(oResponse.get());

	}

	@Test
	void failureExternalTaskWithInvalidTaskIdShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException {
		String taskId = "taskId";
		String workerId = "workerId";
		String errorMessage = "errorMessage";

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(404).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.failureExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaFailureExternalTaskRequestBody.class))).thenReturn(response);

		try {
			workflowService.failureExternalTask(taskId, workerId, errorMessage);
			fail("Should have thrown an BadRequestServiceException.");
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void failureExternalTaskWithValidTaskIdAndInvalidWorkerIdShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException {

		String taskId = "taskId";
		String workerId = "workerId";
		String errorMessage = "errorMessage";

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(400).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.failureExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaFailureExternalTaskRequestBody.class))).thenReturn(response);

		try {
			workflowService.failureExternalTask(taskId, workerId, errorMessage);
			fail("Should have thrown an BadRequestServiceException.");
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void failureExternalTaskWithValidTaskIdAndValidWorkerIdExceptionAccessingWorkflowengineShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {

		String taskId = "taskId";
		String workerId = "workerId";
		String errorMessage = "errorMessage";

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).failureExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
						Mockito.any(CamundaFailureExternalTaskRequestBody.class));

		try {
			workflowService.failureExternalTask(taskId, workerId, errorMessage);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void failureExternalTaskWithValidTaskIdAndValidWorkerIdInternalServerErrorResponseFromWorkflowShouldRespondWithFalse()
			throws InternalErrorServiceException, BadRequestServiceException {
		String taskId = "taskId";
		String workerId = "workerId";
		String errorMessage = "errorMessage";

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(500).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.failureExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaFailureExternalTaskRequestBody.class))).thenReturn(response);
		Optional<Boolean> oResponse = workflowService.failureExternalTask(taskId, workerId, errorMessage);
		assertTrue(oResponse.isPresent());
		assertFalse(oResponse.get());

	}

	@Test
	void failureExternalTaskWithValidTaskIdAndValidWorkerIdInvalidResponseFromWorkflowShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {

		String taskId = "taskId";
		String workerId = "workerId";
		String errorMessage = "errorMessage";

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(406).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.failureExternalTaskInstance(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.any(CamundaFailureExternalTaskRequestBody.class))).thenReturn(response);

		try {
			workflowService.failureExternalTask(taskId, workerId, errorMessage);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void deleteProcessInstanceWithValidBusinessKeyShouldRespondTrue()
			throws InternalErrorServiceException, BadRequestServiceException {
		String businessKey = "businessKey";

		String definitionId = "definitionId";
		String processId = "processId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		pi.setId(processId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(204).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.deleteProcessInstanceWithProcessId(Mockito.anyString(), Mockito.eq(processId), Mockito.eq(true), Mockito.eq(true)))
				.thenReturn(response);

		Optional<Boolean> oResp = workflowService.deleteProcessInstance(businessKey);
		assertTrue(oResp.isPresent());
		assertTrue(oResp.get());

	}

	@Test
	void deleteProcessInstanceWithInvalidBusinessKeyShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException {
		String businessKey = "businessKey";

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(new ArrayList<CamundaProcessInstance>());

		try {
			workflowService.deleteProcessInstance(businessKey);
			fail("Should have thrown an BadRequestServiceException.");
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void deleteProcessInstanceWithValidBusinessKeyExceptionGettingProceessInstanceShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {
		String businessKey = "businessKey";

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi)
				.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(), Mockito.eq(businessKey));

		try {
			workflowService.deleteProcessInstance(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void deleteProcessInstanceWithValidBusinessKeyMoreThanOneProcessInstanceWithSameBusinessKeyShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {
		String businessKey = "businessKey";

		String definitionId = "definitionId";
		String processId = "processId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		pi.setId(processId);
		processInstances.add(pi);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		try {
			workflowService.deleteProcessInstance(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void deleteProcessInstanceWithValidBusinessKeyExceptionDeleteProcessInstanceInWorkflowEngineShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {

		String businessKey = "businessKey";

		String definitionId = "definitionId";
		String processId = "processId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		pi.setId(processId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).deleteProcessInstanceWithProcessId(Mockito.anyString(), Mockito.eq(processId), Mockito.eq(true), Mockito.eq(true));

		try {
			workflowService.deleteProcessInstance(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void deleteProcessInstanceWithValidBusinessKeyNotFoundResponseDeleteProcessInstanceInWorkflowEngineShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException {

		String businessKey = "businessKey";

		String definitionId = "definitionId";
		String processId = "processId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		pi.setId(processId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(404).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.deleteProcessInstanceWithProcessId(Mockito.anyString(), Mockito.eq(processId), Mockito.eq(true), Mockito.eq(true)))
				.thenReturn(response);

		try {
			workflowService.deleteProcessInstance(businessKey);
			fail("Should have thrown an BadRequestServiceException.");
		} catch (BadRequestServiceException e) {
			// pass
		}
	}

	@Test
	void deleteProcessInstanceWithValidBusinessKeyInvalidResponseDeleteProcessInstanceInWorkflowEngineShouldThrowInternalErrorServiceException()
			throws BadRequestServiceException {

		String businessKey = "businessKey";

		String definitionId = "definitionId";
		String processId = "processId";
		List<CamundaProcessInstance> processInstances = new ArrayList<>();
		CamundaProcessInstance pi = new CamundaProcessInstance();
		pi.setBusinessKey(businessKey);
		pi.setProcessDefinitionId(definitionId);
		pi.setId(processId);
		processInstances.add(pi);

		Mockito.when(camundaApi.getProcessInstancesWithBusinessKey(Mockito.anyString(), Mockito.anyString(),
				Mockito.eq(businessKey))).thenReturn(processInstances);

		Response response = Response
				.builder().headers(Collections.emptyMap()).status(406).body("Body".getBytes()).request(Request
						.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(), Body.empty(), null))
				.build();

		Mockito.when(camundaApi.deleteProcessInstanceWithProcessId(Mockito.anyString(), Mockito.eq(processId), Mockito.eq(true), Mockito.eq(true)))
				.thenReturn(response);

		try {
			workflowService.deleteProcessInstance(businessKey);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getWorkflowDefinitionXMLWithValidProcessDefinitionIdShouldRespondWithDefinionValue()
			throws InternalErrorServiceException {
		String processDefinitionId = "processDefinitionId";
		String xmlValue = "xmlValue";
		CamundaProcessDefinitionDiagramXML wfDefinition = new CamundaProcessDefinitionDiagramXML();
		wfDefinition.setBpmn20Xml(xmlValue);
		Mockito.when(camundaApi.getDiagramXML(Mockito.anyString(), Mockito.eq(processDefinitionId)))
				.thenReturn(wfDefinition);

		Optional<String> oXmlValue = workflowService.getWorkflowDefinitionXML(processDefinitionId);
		assertTrue(oXmlValue.isPresent());
		assertEquals(xmlValue, oXmlValue.get());
	}

	@Test
	void getWorkflowDefinitionXMLExceptionAccessingTheWorkflowEngineThrowsInternalErrorServiceException() {
		String processDefinitionId = "processDefinitionId";

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build()))
				.when(camundaApi).getDiagramXML(Mockito.anyString(), Mockito.eq(processDefinitionId));

		try {
			workflowService.getWorkflowDefinitionXML(processDefinitionId);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void hasUserOpertaionAfterTimestampInvalidTimestampThrowsInvalidParameterException() {
		try {
			workflowService.hasUserOperationAfterTimestamp("taskId", null);
			fail("Should have thrown an InternalErrorServiceException.");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void hasUserOpertaionAfterTimestampInvalidTaskIdReturnsFalse() throws InternalErrorServiceException {
		Mockito.when(camundaApi.getUserOpertationLog(Mockito.anyString(), Mockito.eq("invalid"), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(new ArrayList<CamundaUserOperationLog>());
		assertFalse(workflowService.hasUserOperationAfterTimestamp("invalid", ZonedDateTime.now()));
	}

	@Test
	void hasUserOpertaionAfterTimestampValidParametersWithAtLeastOneUserOperationReturnsTrue()
			throws InternalErrorServiceException {
		List<CamundaUserOperationLog> userOperations = new ArrayList<>();
		userOperations.add(new CamundaUserOperationLog());
		Mockito.when(camundaApi.getUserOpertationLog(Mockito.anyString(), Mockito.eq("taskId"), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(userOperations);

		assertTrue(workflowService.hasUserOperationAfterTimestamp("taskId", ZonedDateTime.now()));
	}

	@Test
	void hasUserOpertaionAfterTimestampValidParametersWithNoUserOperationReturnsFalse()
			throws InternalErrorServiceException {
		Mockito.when(camundaApi.getUserOpertationLog(Mockito.anyString(), Mockito.eq("taskId"), Mockito.anyString(),
				Mockito.anyInt())).thenReturn(new ArrayList<CamundaUserOperationLog>());

		assertFalse(workflowService.hasUserOperationAfterTimestamp("taskId", ZonedDateTime.now()));
	}

	@Test
	void getLatestUserOperation() {
		String taskId = "taskId";
		List<CamundaUserOperationLog> userOperations = new ArrayList<>();
		CamundaUserOperationLog userOperation = new CamundaUserOperationLog();
		userOperations.add(userOperation);
		Mockito.when(camundaApi.getLatestUserOpertationsLog(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.eq("timestamp"), Mockito.eq("desc"), Mockito.eq(1))).thenReturn(userOperations);
		Optional<CamundaUserOperationLog> oUserOperation = workflowService.getLatestUserOperation(taskId);
		assertTrue(oUserOperation.isPresent());
		assertEquals(userOperation, oUserOperation.get());
	}

	@Test
	void getLatestUserOperationEmptyListRequrnsOptionalEmpty() {
		String taskId = "taskId";
		List<CamundaUserOperationLog> userOperations = new ArrayList<>();
		Mockito.when(camundaApi.getLatestUserOpertationsLog(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.eq("timestamp"), Mockito.eq("desc"), Mockito.eq(1))).thenReturn(userOperations);
		Optional<CamundaUserOperationLog> oUserOperation = workflowService.getLatestUserOperation(taskId);
		assertFalse(oUserOperation.isPresent());
	}

	@Test
	void touchNewUserOperation() {
		String taskId = "taskId";
		CamundaVariable var = new CamundaVariable();
		var.setLocal(true);
		var.setType("String");
		var.setValue("default");

		workflowService.touchNewUserOperation(taskId);

		Mockito.verify(camundaApi, Mockito.times(1)).putLocalVariable(Mockito.anyString(), Mockito.eq(taskId),
				Mockito.eq("userOperationStore"), Mockito.eq(var));

	}
	
	@Test
	void getLatestWorkflowDefinitionXML() throws InternalErrorServiceException {
		CamundaProcessDefinitionDiagramXML camundaProcessDefDiagXML = new CamundaProcessDefinitionDiagramXML();
		String bpmnxml = "test xml";
		camundaProcessDefDiagXML.setBpmn20Xml(bpmnxml);
		Mockito.when(camundaApi.getLatestDiagramXML(Mockito.anyString(), Mockito.eq(processDefinitionKey), Mockito.eq(tenantId))).thenReturn(camundaProcessDefDiagXML);
		Optional<String> oXml = workflowService.getLatestWorkflowDefinitionXML();
		assertTrue(oXml.isPresent());
		assertEquals(bpmnxml, oXml.get());
	}

	@Test
	void getLatestWorkflowDefinitionXMLNullResponse() throws InternalErrorServiceException {
		CamundaProcessDefinitionDiagramXML camundaProcessDefDiagXML = null;
		Mockito.when(camundaApi.getLatestDiagramXML(Mockito.anyString(), Mockito.eq(processDefinitionKey), Mockito.eq(tenantId))).thenReturn(camundaProcessDefDiagXML);
		Optional<String> oXml = workflowService.getLatestWorkflowDefinitionXML();
		assertFalse(oXml.isPresent());
	}

	@Test
	void getLatestWorkflowDefinitionXMLFeignException() throws InternalErrorServiceException {
		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
						.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
								Body.empty(), null))
						.build())).when(camundaApi).getLatestDiagramXML(Mockito.anyString(), Mockito.eq(processDefinitionKey), Mockito.eq(tenantId));
		try {
			workflowService.getLatestWorkflowDefinitionXML();
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}
	

}
