/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.service.ContactService;
import org.eclipse.openk.statementpublicaffairs.service.ReplacementStringsService;
import org.eclipse.openk.statementpublicaffairs.service.StatementProcessService;
import org.eclipse.openk.statementpublicaffairs.service.StatementService;
import org.eclipse.openk.statementpublicaffairs.service.UsersService;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@EntityScan(basePackageClasses = StatementPublicAffairsApplication.class)
@ContextConfiguration(initializers = { ConfigDataApplicationContextInitializer.class })
@TestPropertySource("spring.config.location=classpath:application-test.yml")
public class TestConfigurationReplacementStringsService {

	@Bean
	public ReplacementStringsService myService() {
		return new ReplacementStringsService();
	}

	@MockBean
	private StatementService statementService;

	@MockBean
	private ContactService contactService;

	@MockBean
	private StatementProcessService statementProcessService;

	@MockBean
	private UsersService usersService;

	@MockBean
	private DepartmentstructureRepository departmentStructureRepository;

}
