/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.statementpublicaffairs.config.auth;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.eclipse.openk.statementpublicaffairs.api.AuthNAuthApi;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationJwtTokenValidationFilter;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;

import feign.Response;
import feign.FeignException;
import feign.Request;
import feign.Request.Body;
import feign.Request.HttpMethod;
import feign.RequestTemplate;

@SpringBootTest(classes = TestConfigurationJwtTokenValidationFilter.class)
@ActiveProfiles("test")
class JwtTokenValidationFilterTest {

	private static final String FIELD_USE_STATIC_JWT = "useStaticJwt";

	@Autowired
	private JwtTokenValidationFilter jwtTokenValidationFilter;
	
	@Autowired
	private AuthNAuthApi authNAuthApi;
	
	private feign.Response mockResponse(int statusCode, String body) {
		feign.Request rq = feign.Request.create(HttpMethod.GET, "http://testurl", new HashMap<>(), new byte[0],
				Charset.defaultCharset(),new RequestTemplate());
		return feign.Response.builder().request(rq).body(body, StandardCharsets.UTF_8).status(statusCode)
				.headers(new HashMap<>()).build();
	}

	@Test
	void testDoFilterInternal() throws ServletException, IOException {

		ReflectionTestUtils.setField(jwtTokenValidationFilter, FIELD_USE_STATIC_JWT, false);
		String token = "TOKEN";
		String bearerToken = "Bearer " + token;

		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		Mockito.when(request.getHeader("Authorization")).thenReturn(bearerToken);

		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
		FilterChain filterChain = Mockito.mock(FilterChain.class);

		Response validResponse = mockResponse(200, "");
		Mockito.when(authNAuthApi.isTokenValid(token)).thenReturn(validResponse);
		
		jwtTokenValidationFilter.doFilterInternal(request, response, filterChain);
		
		Mockito.verify(filterChain).doFilter(request, response);
	}

	@Test
	void testDoFilterInternalInvalidToken() throws ServletException, IOException {

		ReflectionTestUtils.setField(jwtTokenValidationFilter, FIELD_USE_STATIC_JWT, false);

		String token = "TOKEN";
		String bearerToken = "Bearer " + token;

		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		Mockito.when(request.getHeader("Authorization")).thenReturn(bearerToken);

		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
		FilterChain filterChain = Mockito.mock(FilterChain.class);

		Response validResponse = mockResponse(401, "");
		Mockito.when(authNAuthApi.isTokenValid(token)).thenReturn(validResponse);
		
		jwtTokenValidationFilter.doFilterInternal(request, response, filterChain);
		
		Mockito.verify(filterChain).doFilter(request, response);
		Mockito.verify(response).sendError(Mockito.eq(401), Mockito.anyString());
		
	}
	
	@Test
	void testDoFilterInternalAuthNAuthServiceNoConclusiveResponse() throws ServletException, IOException {

		ReflectionTestUtils.setField(jwtTokenValidationFilter, FIELD_USE_STATIC_JWT, false);

		String token = "TOKEN";
		String bearerToken = "Bearer " + token;

		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		Mockito.when(request.getHeader("Authorization")).thenReturn(bearerToken);

		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
		FilterChain filterChain = Mockito.mock(FilterChain.class);

		Response validResponse = mockResponse(0, "");
		Mockito.when(authNAuthApi.isTokenValid(token)).thenReturn(validResponse);
		
		jwtTokenValidationFilter.doFilterInternal(request, response, filterChain);
		
		Mockito.verify(filterChain).doFilter(request, response);
		Mockito.verify(response).sendError(Mockito.eq(401), Mockito.anyString());
		
	}

	@Test
	void testDoFilterInternalAuthNAuthServiceFeignException() throws ServletException, IOException {

		ReflectionTestUtils.setField(jwtTokenValidationFilter, FIELD_USE_STATIC_JWT, false);

		String token = "TOKEN";
		String bearerToken = "Bearer " + token;

		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		Mockito.when(request.getHeader("Authorization")).thenReturn(bearerToken);

		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
		FilterChain filterChain = Mockito.mock(FilterChain.class);

		Mockito.doThrow(FeignException.errorStatus("",
				Response.builder().headers(Collections.emptyMap()).status(400)
				.request(Request.create(Request.HttpMethod.GET, "", new HashMap<String, Collection<String>>(),
						Body.empty(), new RequestTemplate()))
				.build())).when(authNAuthApi).isTokenValid(token);
		
		jwtTokenValidationFilter.doFilterInternal(request, response, filterChain);
		
		Mockito.verify(filterChain).doFilter(request, response);
		Mockito.verify(response).sendError(Mockito.eq(401), Mockito.anyString());
		
	}
	

}
