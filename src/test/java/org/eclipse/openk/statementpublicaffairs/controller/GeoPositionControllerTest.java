/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.model.Position;
import org.eclipse.openk.statementpublicaffairs.service.GeoPositionService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class GeoPositionControllerTest {

	@MockBean
	private GeoPositionService geoPositionService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void testTransform() throws Exception {

		String from = "EPSG:4326";
		String to = "EPSG:25832";

		Map<String, Position> transformed = new HashMap<>();
		Mockito.when(geoPositionService.transform(Mockito.anyMap(), Mockito.eq(from), Mockito.eq(to)))
				.thenReturn(transformed);

		String content = "{}";
		mockMvc.perform(post("/geo-coordinate-transform?from=" + from + "&to=" + to).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(content))
				.andExpect(status().is2xxSuccessful()).andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

}
