*******************************************************************************
  Copyright (c) 2019 Contributors to the Eclipse Foundation

  See the NOTICE file(s) distributed with this work for additional
  information regarding copyright ownership.

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License v. 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
*******************************************************************************

# Demo Reverse Proxy for all openKONSEQUENZ services

This docker configuration creates a nginx based reverse-proxy.
It references docker services that are not part of this module.

Please use the docker-compose from the project root folder to build this docker image.

## Redirect configuration

Requests are redirected to the specific dockerized services according to the default.conf configuration.

