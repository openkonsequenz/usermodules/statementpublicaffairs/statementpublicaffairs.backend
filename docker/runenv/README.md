*******************************************************************************
  Copyright (c) 2019 Contributors to the Eclipse Foundation

  See the NOTICE file(s) distributed with this work for additional
  information regarding copyright ownership.

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License v. 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
*******************************************************************************

# Statement service backend docker 

This docker configuration creates a docker image for the statement module back end.

Before creating the docker image, you have to build the statement back end jar first.
The folder ../buildenv contains a dockerized build environment for that.

Please use the docker-compose from the project root folder to build this docker image.

## Configuration

The sub-folder "config" contains all configuration files and is added to the docker image at build time.

In production you may not want you mail access credentials included in the docker image. Therefore just
mount a config docker-volume or local folder containing all of the config files to the /app/config folder
of the docker instance.


