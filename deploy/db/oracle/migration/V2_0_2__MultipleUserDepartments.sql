-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2022 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------


-- create sequence user2department_id_seq

CREATE SEQUENCE tbl_user2department_id_seq
    START WITH 1
    INCREMENT BY 1
    NOMAXVALUE
    NOMINVALUE
    NOCACHE;
   
-- create table user2department 

CREATE TABLE tbl_user2department (
    id NUMBER(10) DEFAULT tbl_user2department_id_seq.nextval NOT NULL,
    user_id NUMBER(10) NOT NULL,
    department_id NUMBER(10) NOT NULL,
    stand_in NUMBER(1) NOT NULL
);

ALTER TABLE tbl_user2department ADD
    CONSTRAINT tbl_user2department_pk PRIMARY KEY (id);

ALTER TABLE tbl_user2department ADD
    CONSTRAINT tbl_user2department_uk UNIQUE (user_id, department_id);

-- public.tbl_user2department foreign keys

ALTER TABLE tbl_user2department 
    ADD CONSTRAINT tbl_user2department_fk_1 FOREIGN KEY (user_id) REFERENCES tbl_user(id);
ALTER TABLE tbl_user2department ADD CONSTRAINT tbl_user2department_fk_2 FOREIGN KEY (department_id) REFERENCES tbl_department(id);

-- create entries for existing

INSERT into tbl_user2department (user_id, department_id, stand_in) select id, department_id , 0 from tbl_user where department_id is not null;

-- update view vw_statement_reqdepartment_users, replace user.department_id with ref via tbl_user2department

CREATE OR REPLACE VIEW vw_statement_reqdepartment_users
AS SELECT row_number() OVER (ORDER BY u.id) AS id,
    u.id AS user_id,
    u.first_name,
    u.last_name,
    u.username AS user_name,
    u.email_address,
    wd.statement_id,
    wd.id AS workflow_id,
    ud.department_id,
    d.name AS department_name,
    d.departmentgroup AS department_group,
    rd.contributed AS department_contributed,
    rd.optional AS department_optional,
    ud.stand_in as stand_in
   FROM tbl_user2department ud
     JOIN tbl_user u ON ud.user_id  = u.id
     JOIN tbl_department d ON ud.department_id = d.id
     JOIN tbl_reqdepartment rd ON rd.department_id = d.id
     JOIN tbl_workflowdata wd ON wd.id = rd.workflowdata_id;


-- rm reference from user to department

ALTER table tbl_user DROP COLUMN department_id;
