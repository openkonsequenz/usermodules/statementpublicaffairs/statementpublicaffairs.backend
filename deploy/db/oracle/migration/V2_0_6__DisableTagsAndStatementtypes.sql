-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2022 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

-- disable tags: add standard flag for tags not allowed to disable and disabled flag
ALTER TABLE tbl_tag ADD standard NUMBER(1) DEFAULT 0 NOT NULL;
ALTER TABLE tbl_tag ADD disabled NUMBER(1) DEFAULT 0 NOT NULL;

ALTER TABLE tbl_tag ADD CONSTRAINT tbl_tag_name_u UNIQUE (name);

UPDATE tbl_tag set standard = 1 WHERE id IN ('email-text', 'email', 'outbox', 'consideration', 'statement', 'cover-letter', 'overview', 'expertise', 'plan', 'explanatory-report');


-- disable statementtype: add disabled flag

ALTER TABLE tbl_statementtype ADD disabled NUMBER(1) DEFAULT 0 NOT NULL;

ALTER TABLE tbl_statementtype ADD CONSTRAINT tbl_statementtype_name_u UNIQUE (name);


CREATE OR REPLACE VIEW vw_deletable_tag AS
SELECT * from tbl_tag where id not in (select distinct t.id from tbl_attachment2tag tat join tbl_tag t on t.id = tat.tag_id) and standard != 1 and disabled = 1;


CREATE OR REPLACE VIEW vw_deletable_statementtype AS
SELECT * from tbl_statementtype ts where id not in (select distinct st.id from tbl_statement s join tbl_statementtype st  on st.id = s.type_id) and disabled = 1;


-- increment id sequence by 5 to ensure valid nextval

SELECT tbl_statementtype_id_seq.nextval FROM dual;
SELECT tbl_statementtype_id_seq.nextval FROM dual;
SELECT tbl_statementtype_id_seq.nextval FROM dual;
SELECT tbl_statementtype_id_seq.nextval FROM dual;
SELECT tbl_statementtype_id_seq.nextval FROM dual;
