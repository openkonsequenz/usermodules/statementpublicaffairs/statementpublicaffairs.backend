-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2022 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

-- create sequence tbl_statementdraft_id_seq

CREATE SEQUENCE tbl_statementdraft_id_seq
    START WITH 1
    INCREMENT BY 1
    NOMAXVALUE
    NOMINVALUE
    NOCACHE;


-- create table tbl_statementdraft 

CREATE TABLE tbl_statementdraft (
    id NUMBER(10) DEFAULT tbl_statementdraft_id_seq.nextval NOT NULL,
    workflow_id NUMBER(10) NOT NULL,
    user_id NUMBER(10),
    draft CLOB NOT NULL,
    msg character varying(255),
    ts timestamp NOT NULL
);

ALTER TABLE tbl_statementdraft ADD
    CONSTRAINT tbl_statementdraft_pk PRIMARY KEY (id);

-- public.tbl_statementdraft foreign keys

ALTER TABLE tbl_statementdraft ADD CONSTRAINT tbl_statementdraft_fk_1 FOREIGN KEY (workflow_id) REFERENCES tbl_workflowdata(id);
ALTER TABLE tbl_statementdraft ADD CONSTRAINT tbl_statementdraft_fk_2 FOREIGN KEY (user_id) REFERENCES tbl_user(id);

-- copy existing values

INSERT into tbl_statementdraft (workflow_id, draft, ts, msg) select id, draft , current_timestamp, 'initial-migrate'  from tbl_workflowdata where draft is not null;

-- view

CREATE OR REPLACE VIEW vw_statementdraft_history
AS select sd.id as id, row_number() over (PARTITION by sd.workflow_id order by sd.id) as v, wd.statement_id as statement_id, sd.workflow_id as workflow_id, sd.user_id as user_id, u.username as username, u.first_name as user_first_name, u.last_name as user_last_name, sd.ts as ts, sd.msg as msg, sd.draft as draft from tbl_statementdraft sd join tbl_workflowdata wd on wd.id = sd.workflow_id left join tbl_user u on u.id = sd.user_id;


-- delete column workflowdata draft entries

ALTER table tbl_workflowdata DROP COLUMN draft;
